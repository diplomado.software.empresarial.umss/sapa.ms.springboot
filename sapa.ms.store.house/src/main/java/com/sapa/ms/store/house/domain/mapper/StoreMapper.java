package com.sapa.ms.store.house.domain.mapper;

import com.sapa.ms.store.house.domain.dto.ItemRes;
import com.sapa.ms.store.house.domain.dto.StoreReq;
import com.sapa.ms.store.house.domain.dto.StoreRes;
import com.sapa.ms.store.house.domain.entities.Item;
import com.sapa.ms.store.house.domain.entities.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class StoreMapper {
    @Autowired
    private static ItemMapper itemMapper;

    public static StoreRes toDto(Store entity) {
        StoreRes res = new StoreRes();
        res.setId(entity.getId());
        res.setName(entity.getName());
        res.setAddress(entity.getAddress());
        res.setItemList(getItems(entity.getItems()));
        return res;
    }

    public static Store toEntity(StoreReq req) {
        Store entity = new Store();
        entity.setManagerId(req.getManagerId());
        entity.setName(req.getName());
        entity.setAddress(req.getAddress());
        return entity;
    }

    public static Store merge(Store entity, StoreReq req) {
        entity.setManagerId(req.getManagerId());
        entity.setName(req.getName());
        entity.setAddress(req.getAddress());
        return entity;
    }

    private static List<ItemRes> getItems(List<Item> itemSet) {
        List<ItemRes> itemList = new ArrayList<>();
        if (itemSet != null && !itemSet.isEmpty()) {
            itemList = itemSet
                    .stream()
                    .map(item -> itemMapper.toDto(item))
                    .collect(Collectors.toList());
        }
        return itemList;
    }
}
