package com.sapa.ms.store.house.domain.mapper;

import com.sapa.ms.store.house.domain.dto.AssignmentProjectReq;
import com.sapa.ms.store.house.domain.dto.AssignmentProjectRes;
import com.sapa.ms.store.house.domain.entities.AssignmentProject;
import com.sapa.ms.store.house.domain.entities.Item;
import com.sapa.ms.store.house.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Component
public class AssignmentProjectMapper {
    @Autowired
    private ItemRepository itemRepository;

    public AssignmentProject toEntity(AssignmentProjectReq req) throws Exception {
        AssignmentProject entity = new AssignmentProject();
        entity.setUserId(req.getUserId());
        entity.setProjectId(req.getProjectId());
        entity.setItem(findItemById(req.getItemId()));
        entity.setUnits(req.getUnits());
        entity.setDate(stringToLocalDate(req.getDate()));
        entity.setComments(req.getComments());
        return entity;
    }

    public AssignmentProjectRes toDto(AssignmentProject entity) {
        AssignmentProjectRes res = new AssignmentProjectRes();
        res.setId(entity.getId());
        res.setItemId(entity.getItem().getId());
        res.setUnits(entity.getUnits());
        res.setDate(localDateToString(entity.getDate()));
        res.setComments(entity.getComments());
        return res;
    }

    public AssignmentProject merge(AssignmentProject entity, AssignmentProjectReq req) throws Exception {
        entity.setUserId(req.getUserId());
        entity.setProjectId(req.getProjectId());
        entity.setItem(findItemById(req.getItemId()));
        entity.setUnits(req.getUnits());
        entity.setDate(stringToLocalDate(req.getDate()));
        entity.setComments(req.getComments());
        return entity;
    }

    private Item findItemById(Long id) throws Exception {
        Optional<Item> itemOptional = itemRepository.findById(id);
        if (itemOptional.isEmpty()) {
            throw new Exception("AssignmentProjectMapper: there is not found item with id " + id);
        }
        return itemOptional.get();
    }

    private static String localDateToString(LocalDate date) {
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatters);
    }

    private static LocalDate stringToLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(dateString, formatter);
        return date;
    }
}
