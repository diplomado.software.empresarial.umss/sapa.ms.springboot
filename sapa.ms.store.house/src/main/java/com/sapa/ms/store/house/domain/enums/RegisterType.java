package com.sapa.ms.store.house.domain.enums;

public enum RegisterType {
    INPUT_ITEM, OUTPUT_ITEM
}
