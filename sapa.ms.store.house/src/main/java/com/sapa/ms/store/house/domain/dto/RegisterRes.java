package com.sapa.ms.store.house.domain.dto;

import com.sapa.ms.store.house.domain.domainMS.user.dto.UserRes;
import lombok.Setter;

@Setter
public class RegisterRes {
    public Long id;
    public String date;
    public Integer units;
    public String type;
    public String comments;
    public Long assignmentProjectId;
    public Long itemId;
    public UserRes userRes;
}
