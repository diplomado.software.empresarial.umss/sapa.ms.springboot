package com.sapa.ms.store.house.domain.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;

@Getter
public class StoreReq {
    @NotNull(message = "field manager id cant not be null")
    public Long managerId;
    @NotNull(message = "field name cant not be null")
    public String name;
    @NotNull(message = "field address cant not be null")
    public String address;
}
