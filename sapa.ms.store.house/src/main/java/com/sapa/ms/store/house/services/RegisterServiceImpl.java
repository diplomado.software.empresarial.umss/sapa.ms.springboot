package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.domainMS.user.controller.UserControllerMS;
import com.sapa.ms.store.house.domain.domainMS.user.dto.UserRes;
import com.sapa.ms.store.house.domain.dto.RegisterReq;
import com.sapa.ms.store.house.domain.dto.RegisterRes;
import com.sapa.ms.store.house.domain.entities.Item;
import com.sapa.ms.store.house.domain.entities.Register;
import com.sapa.ms.store.house.domain.enums.RegisterType;
import com.sapa.ms.store.house.domain.mapper.RegisterMapper;
import com.sapa.ms.store.house.repositories.ItemRepository;
import com.sapa.ms.store.house.repositories.RegisterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    private RegisterRepository registerRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ItemService itemService;
    @Autowired
    private RegisterMapper registerMapper;
    @Autowired
    private UserControllerMS userControllerMS;

    @Override
    public RegisterRes findById(Long id) throws Exception {
        Register entity = findRegisterById(id);
        RegisterRes res = registerMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUserRes(userRes);
        return res;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public RegisterRes create(RegisterReq req) throws Exception {
        Register entity = registerMapper.toEntity(req);
        updateStockFromItem(entity);
        entity = registerRepository.save(entity);
        RegisterRes res = registerMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUserRes(userRes);
        return res;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public RegisterRes update(Long id, RegisterReq req) throws Exception {
        Register entity = findRegisterById(id);
        restoreStockFromItem(entity);
        entity = registerMapper.merge(entity, req);
        updateStockFromItem(entity);
        registerRepository.save(entity);
        RegisterRes res = registerMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUserRes(userRes);
        return res;
    }

    @Override
    public List<RegisterRes> findRegisterByItemId(Long id) throws Exception {
        Item item = findItemById(id);
        List<Register> registerList = registerRepository.findRegisterByItemId(item.getId());
        return registerList
                .stream()
                .map(register -> {
                    RegisterRes res = registerMapper.toDto(register);
                    try {
                        UserRes userRes = userControllerMS.findById(register.getUserId());
                        res.setUserRes(userRes);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    return res;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<RegisterRes> findRegisterByItemIdAndType(Long id, String type) throws Exception {
        Item item = findItemById(id);
        List<Register> registerList = registerRepository.findRegisterByItemIdAndType(item.getId(), type);
        return registerList
                .stream()
                .map(register -> {
                    RegisterRes res = registerMapper.toDto(register);
                    try {
                        UserRes userRes = userControllerMS.findById(register.getUserId());
                        res.setUserRes(userRes);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                    return res;
                })
                .collect(Collectors.toList());
    }

    @Override
    public Register findRegisterByAssignmentProjectId(Long id) throws Exception {
        Optional<Register> registerOptional = registerRepository.findRegisterByAssignmentProjectId(id);
        if (registerOptional.isEmpty()) {
            throw new Exception("There is not found register with id from AssignmentProject" + id);
        }
        Register entity = registerOptional.get();
        return entity;
    }

    private Register findRegisterById(Long id) throws Exception {
        Optional<Register> registerOptional = registerRepository.findRegisterById(id);
        if (registerOptional.isEmpty()) {
            throw new Exception("There is not found register with id " + id);
        }
        Register entity = registerOptional.get();
        return entity;
    }

    private Item findItemById(Long id) throws Exception {
        Optional<Item> itemOptional = itemRepository.findById(id);
        if (itemOptional.isEmpty()) {
            throw new Exception("RegisterService: there is not found item with id " + id);
        }
        Item entity = itemOptional.get();
        return entity;
    }

    private void restoreStockFromItem(Register register) throws Exception {
        Long idItem = register.getItem().getId();
        if (register.getType().equals(RegisterType.OUTPUT_ITEM.toString())) {
            itemService.addToStock(idItem, register.getUnits());
        } else {
            itemService.removeFromStock(idItem, register.getUnits());
        }
    }

    private void updateStockFromItem(Register register) throws Exception {
        Long idItem = register.getItem().getId();
        if (register.getType().equals(RegisterType.INPUT_ITEM.toString())) {
            itemService.addToStock(idItem, register.getUnits());
        } else {
            itemService.removeFromStock(idItem, register.getUnits());
        }
    }
}
