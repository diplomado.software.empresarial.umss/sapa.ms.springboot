package com.sapa.ms.store.house.domain.domainMS.user.controller;

import com.sapa.ms.store.house.domain.domainMS.user.dto.ProjectRes;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "store-house-ms", url = "localhost:8094/project/ms/project")
public interface ProjectControllerMS {
    @GetMapping("/{id}")
    ProjectRes findById(@Valid @PathVariable(name = "id") Long id) throws Exception;
}
