package com.sapa.ms.store.house.repositories;


import com.sapa.ms.store.house.domain.entities.Register;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RegisterRepository extends JpaRepository<Register, Long> {
    Optional<Register> findRegisterById(Long id);

    List<Register> findRegisterByItemId(Long id);

    List<Register> findRegisterByItemIdAndType(Long id, String type);

    Optional<Register> findRegisterByAssignmentProjectId(Long id);
}
