package com.sapa.ms.store.house.domain.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;

@Getter
public class ItemReq {
    @NotNull(message = "field name id cant not be null")
    public String name;
    @NotNull(message = "field unit of measurement id cant not be null")
    public String unitOfMeasurement;
    public Integer stock;
    @NotNull(message = "field store id id cant not be null")
    public Long storeId;
}
