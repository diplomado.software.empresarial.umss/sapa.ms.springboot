package com.sapa.ms.store.house.domain.dto;

import jakarta.validation.constraints.NotNull;
import lombok.Getter;

@Getter
public class AssignmentProjectReq {
    @NotNull(message = "field userId cant not be null")
    public Long userId; // user-mss
    @NotNull(message = "field projectId cant not be null")
    public Long projectId; // project-ms
    @NotNull(message = "field itemId cant not be null")
    public Long itemId;
    @NotNull(message = "field units cant not be null")
    public Integer units;
    @NotNull(message = "field date cant not be null")
    public String date;
    @NotNull(message = "field comments cant not be null")
    public String comments;
}
