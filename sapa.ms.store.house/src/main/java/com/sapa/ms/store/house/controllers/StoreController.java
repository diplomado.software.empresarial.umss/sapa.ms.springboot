package com.sapa.ms.store.house.controllers;

import com.sapa.ms.store.house.domain.dto.StoreReq;
import com.sapa.ms.store.house.domain.dto.StoreRes;
import com.sapa.ms.store.house.services.StoreService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("store/house/ms/store")
@Tag(name = "STORE-HOUSE-MS API")
public class StoreController {
    @Autowired
    private StoreService storeService;

    /**
     * @param id
     * @return ResponseEntity<StoreRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<StoreRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        StoreRes storeRes = storeService.findById(id);
        return ResponseEntity.ok().body(storeRes);
    }

    /**
     * @param req
     * @return ResponseEntity<StoreRes>
     */
    @PostMapping("")
    public ResponseEntity<StoreRes> create(@Valid @RequestBody StoreReq req) throws Exception {
        StoreRes storeRes = storeService.create(req);
        return ResponseEntity.ok().body(storeRes);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<StoreRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<StoreRes> update(@Valid @PathVariable(name = "id") Long id,
                                           @Valid @RequestBody StoreReq req) throws Exception {
        StoreRes storeRes = storeService.update(id, req);
        return ResponseEntity.ok().body(storeRes);
    }

    /**
     * @param id
     * @return ResponseEntity<Void>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        storeService.delete(id);
        return ResponseEntity.ok().body(null);
    }

    /**
     * @return ResponseEntity<List < StoreRes>>
     */
    @GetMapping("/list")
    public ResponseEntity<List<StoreRes>> getStoreList() {
        List<StoreRes> storeList = storeService.getStoreList();
        return ResponseEntity.ok().body(storeList);
    }
}
