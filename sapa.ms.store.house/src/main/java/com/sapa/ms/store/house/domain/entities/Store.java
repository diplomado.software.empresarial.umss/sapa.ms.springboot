package com.sapa.ms.store.house.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "store")
public class Store {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "manager_id")
    private Long managerId;
    private String name;
    private String address;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "store")
    private List<Item> items;
}
