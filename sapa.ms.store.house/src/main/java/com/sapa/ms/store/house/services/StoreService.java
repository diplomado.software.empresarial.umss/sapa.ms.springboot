package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.dto.StoreReq;
import com.sapa.ms.store.house.domain.dto.StoreRes;
import com.sapa.ms.store.house.domain.entities.Store;

import java.util.List;

public interface StoreService {
    StoreRes findById(Long id) throws Exception;

    StoreRes create(StoreReq req) throws Exception;

    StoreRes update(Long id, StoreReq req) throws Exception;

    void delete(Long id) throws Exception;

    List<StoreRes> getStoreList();

    Store findStoreById(Long id) throws Exception;
}
