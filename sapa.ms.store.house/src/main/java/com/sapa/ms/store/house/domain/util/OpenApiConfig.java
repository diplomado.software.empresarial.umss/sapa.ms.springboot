package com.sapa.ms.store.house.domain.util;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "API FOR STORE-HOUSE-MS",
                version = "1.0.0",
                description = "API for Store House Microservice from SAPA"
        )
)
public class OpenApiConfig {

}
