package com.sapa.ms.store.house.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String name;
    @Column(name = "unit")
    private String unitOfMeasurement;
    private Integer stock;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "item")
    private List<AssignmentProject> assignmentProjectList;
    @ManyToOne()
    @JoinColumn(name = "store_id", nullable = false)
    private Store store;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "item")
    private List<Register> register;
}
