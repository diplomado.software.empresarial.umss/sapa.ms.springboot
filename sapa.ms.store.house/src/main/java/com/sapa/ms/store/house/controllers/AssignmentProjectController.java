package com.sapa.ms.store.house.controllers;

import com.sapa.ms.store.house.domain.dto.AssignmentProjectReq;
import com.sapa.ms.store.house.domain.dto.AssignmentProjectRes;
import com.sapa.ms.store.house.domain.entities.AssignmentProject;
import com.sapa.ms.store.house.services.AssignmentProjectService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("store/house/ms/assignment")
@Tag(name = "ASSIGNMENT-MS API")
public class AssignmentProjectController {
    @Autowired
    private AssignmentProjectService assignmentProjectService;

    /**
     * @param id
     * @return ResponseEntity<AssignmentProjectRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<AssignmentProjectRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        AssignmentProjectRes assignment = assignmentProjectService.findById(id);
        return ResponseEntity.ok().body(assignment);
    }

    /**
     * @param id
     * @return List<AssignmentProjectRes>
     * @throws Exception
     */
    @GetMapping("/project/{id}")
    public ResponseEntity<List<AssignmentProjectRes>> findByProjectId(@Valid @PathVariable(name = "id") Long id) throws Exception {
        List<AssignmentProjectRes> list = assignmentProjectService.findByProjectId(id);
        return ResponseEntity.ok().body(list);
    }

    /**
     * @param req
     * @return ResponseEntity<AssignmentProjectRes>
     * @throws Exception
     */
    @PostMapping("")
    public ResponseEntity<AssignmentProjectRes> create(@Valid @RequestBody AssignmentProjectReq req) throws Exception {
        AssignmentProjectRes assignment = assignmentProjectService.create(req);
        return ResponseEntity.ok().body(assignment);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<AssignmentProjectRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<AssignmentProjectRes> update(@Valid @PathVariable(name = "id") Long id,
                                                       @Valid @RequestBody AssignmentProjectReq req) throws Exception {
        AssignmentProjectRes assignment = assignmentProjectService.update(id, req);
        return ResponseEntity.ok().body(assignment);
    }

    /**
     * @param id
     * @return ResponseEntity<Void>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        assignmentProjectService.delete(id);
        return ResponseEntity.ok().body(null);
    }

    /**
     * @param id
     * @return ResponseEntity<List < AssignmentProjectRes>>
     */
    @GetMapping("/list/{id}")
    public ResponseEntity<List<AssignmentProjectRes>> findAssignmentByItemId(@Valid @PathVariable(name = "id") Long id) {
        List<AssignmentProjectRes> assignmentList = assignmentProjectService.findAssignmentByItemId(id);
        return ResponseEntity.ok().body(assignmentList);
    }
}
