package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.dto.ItemReq;
import com.sapa.ms.store.house.domain.dto.ItemRes;
import com.sapa.ms.store.house.domain.entities.Item;

import java.util.List;

public interface ItemService {
    ItemRes findById(Long id) throws Exception;

    ItemRes create(ItemReq req) throws Exception;

    ItemRes update(Long id, ItemReq req) throws Exception;

    void delete(Long id) throws Exception;

    List<ItemRes> getItemListByStoreId(Long idStore);

    List<ItemRes> findAll();

    void addToStock(Long id,Integer units) throws Exception;

    void removeFromStock(Long id,Integer units) throws Exception;
}
