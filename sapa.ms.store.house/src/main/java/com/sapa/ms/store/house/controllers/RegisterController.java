package com.sapa.ms.store.house.controllers;

import com.sapa.ms.store.house.domain.dto.RegisterReq;
import com.sapa.ms.store.house.domain.dto.RegisterRes;
import com.sapa.ms.store.house.services.RegisterService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("store/house/ms/register")
@Tag(name = "REGISTER-MS API")
public class RegisterController {
    @Autowired
    private RegisterService registerService;

    /**
     * @param id
     * @return ResponseEntity<RegisterRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<RegisterRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        RegisterRes register = registerService.findById(id);
        return ResponseEntity.ok().body(register);
    }

    /**
     * @param req
     * @return ResponseEntity<RegisterRes>
     * @throws Exception
     */
    @PostMapping("")
    public ResponseEntity<RegisterRes> create(@Valid @RequestBody RegisterReq req) throws Exception {
        RegisterRes register = registerService.create(req);
        return ResponseEntity.ok().body(register);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<RegisterRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<RegisterRes> update(@Valid @PathVariable Long id,
                                              @Valid @RequestBody RegisterReq req) throws Exception {
        RegisterRes register = registerService.update(id, req);
        return ResponseEntity.ok().body(register);
    }

    /**
     * @param id
     * @return ResponseEntity<List < RegisterRes>>
     * @throws Exception
     */
    @GetMapping("/list/item/{id}")
    public ResponseEntity<List<RegisterRes>> findRegisterByItemId(@Valid @PathVariable Long id) throws Exception {
        List<RegisterRes> registerList = registerService.findRegisterByItemId(id);
        return ResponseEntity.ok().body(registerList);
    }

    /**
     * @param idItem
     * @param type
     * @return ResponseEntity<List < RegisterRes>>
     * @throws Exception
     */
    @GetMapping("/list/{idItem}/{type}")
    public ResponseEntity<List<RegisterRes>> findRegisterByItemAndType(@Valid @PathVariable(name = "idItem") Long idItem,
                                                                       @Valid @PathVariable(name = "type") String type) throws Exception {
        List<RegisterRes> registerList = registerService.findRegisterByItemIdAndType(idItem, type);
        return ResponseEntity.ok().body(registerList);
    }
}
