package com.sapa.ms.store.house.domain.domainMS.user.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectRes {
    public Long id;
    public String name;
    public String state;
    public String startDate;
    public String endDate;
    public String description;
    public UserRes manager;
}
