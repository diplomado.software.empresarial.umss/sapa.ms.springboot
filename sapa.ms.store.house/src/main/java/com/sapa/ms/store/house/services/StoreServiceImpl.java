package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.domainMS.user.controller.UserControllerMS;
import com.sapa.ms.store.house.domain.domainMS.user.dto.UserRes;
import com.sapa.ms.store.house.domain.dto.StoreReq;
import com.sapa.ms.store.house.domain.dto.StoreRes;
import com.sapa.ms.store.house.domain.entities.Store;
import com.sapa.ms.store.house.domain.mapper.StoreMapper;
import com.sapa.ms.store.house.repositories.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private StoreRepository storeRepository;
    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private UserControllerMS userControllerMS;

    @Override
    public StoreRes findById(Long id) throws Exception {
        Store entity = findStoreById(id);
        StoreRes res = storeMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getManagerId());
        res.setManager(userRes);
        return res;
    }

    @Override
    public StoreRes create(StoreReq req) throws Exception {
        Store store = storeMapper.toEntity(req);
        store = storeRepository.save(store);
        StoreRes res = storeMapper.toDto(store);
        UserRes userRes = userControllerMS.findById(store.getManagerId());
        res.setManager(userRes);
        return res;
    }

    @Override
    public StoreRes update(Long id, StoreReq req) throws Exception {
        Store store = findStoreById(id);
        store = storeMapper.merge(store, req);
        store = storeRepository.save(store);
        StoreRes res = storeMapper.toDto(store);
        UserRes userRes = userControllerMS.findById(store.getManagerId());
        res.setManager(userRes);
        return res;
    }

    @Override
    public void delete(Long id) throws Exception {
        Store store = findStoreById(id);

        storeRepository.delete(store);
    }

    @Override
    public List<StoreRes> getStoreList() {
        List<Store> storeList = storeRepository.findAll();
        return storeList
                .stream()
                .map(entity -> {
                    try {
                        StoreRes res =  storeMapper.toDto(entity);
                        UserRes userRes = userControllerMS.findById(entity.getManagerId());
                        res.setManager(userRes);
                        return res;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public Store findStoreById(Long id) throws Exception {
        Optional<Store> storeOptional = storeRepository.findStoreById(id);
        if (storeOptional.isEmpty()) {
            throw new Exception("There is not found store with id " + id);
        }
        return storeOptional.get();
    }
}
