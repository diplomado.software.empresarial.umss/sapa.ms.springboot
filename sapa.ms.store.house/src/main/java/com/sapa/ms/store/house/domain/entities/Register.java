package com.sapa.ms.store.house.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Data
@Entity
@Table(name = "register")
public class Register {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user_id")
    private Long userId; // user-ms
    private LocalDate date;
    private Integer units;
    private String type;
    private String comments;
    @ManyToOne
    @JoinColumn(name = "item_id", nullable = false)
    private Item item;
    @OneToOne
    @JoinColumn(name = "assignment_project_id", referencedColumnName = "id")
    private AssignmentProject assignmentProject;
}
