package com.sapa.ms.store.house.repositories;


import com.sapa.ms.store.house.domain.entities.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    Optional<Item> findItemById(Long id);

    List<Item> findItemByStoreId(Long idStore);
}
