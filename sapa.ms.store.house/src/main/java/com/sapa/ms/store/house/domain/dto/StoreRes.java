package com.sapa.ms.store.house.domain.dto;

import com.sapa.ms.store.house.domain.domainMS.user.dto.UserRes;
import lombok.Setter;

import java.util.List;

@Setter
public class StoreRes {
    public Long id;
    public String name;
    public String address;
    public List<ItemRes> itemList;
    public UserRes manager;
}
