package com.sapa.ms.store.house.domain.mapper;

import com.sapa.ms.store.house.domain.dto.ItemReq;
import com.sapa.ms.store.house.domain.dto.ItemRes;
import com.sapa.ms.store.house.domain.entities.Item;
import org.springframework.stereotype.Component;

@Component
public class ItemMapper {
    public static ItemRes toDto(Item item) {
        ItemRes res = new ItemRes();
        res.setId(item.getId());
        res.setName(item.getName());
        res.setUnitOfMeasurement(item.getUnitOfMeasurement());
        res.setStock(item.getStock());
        res.setStoreId(item.getStore().getId());
        return res;
    }

    public static Item toEntity(ItemReq req) {
        Item item = new Item();
        item.setName(req.getName());
        item.setUnitOfMeasurement(req.getUnitOfMeasurement());
        item.setStock((req.getStock()==null)?0:req.getStock());
        return item;
    }

    public static Item merge(Item item, ItemReq req) {
        item.setName(req.getName());
        item.setUnitOfMeasurement(req.getUnitOfMeasurement());
        item.setStock(req.getStock());
        return item;
    }
}
