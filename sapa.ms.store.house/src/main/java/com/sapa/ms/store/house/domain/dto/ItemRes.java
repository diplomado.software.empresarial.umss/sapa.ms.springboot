package com.sapa.ms.store.house.domain.dto;

import lombok.Setter;

@Setter
public class ItemRes {
    public Long id;
    public String name;
    public String unitOfMeasurement;
    public Integer stock;
    public Long storeId;
}
