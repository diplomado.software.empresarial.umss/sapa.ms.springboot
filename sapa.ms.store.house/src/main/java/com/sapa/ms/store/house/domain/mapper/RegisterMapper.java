package com.sapa.ms.store.house.domain.mapper;

import com.sapa.ms.store.house.domain.dto.RegisterReq;
import com.sapa.ms.store.house.domain.dto.RegisterRes;
import com.sapa.ms.store.house.domain.entities.AssignmentProject;
import com.sapa.ms.store.house.domain.entities.Item;
import com.sapa.ms.store.house.domain.entities.Register;
import com.sapa.ms.store.house.domain.enums.RegisterType;
import com.sapa.ms.store.house.repositories.AssignmentProjectRepository;
import com.sapa.ms.store.house.repositories.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;


@Component
public class RegisterMapper {
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private AssignmentProjectRepository assignmentProjectRepository;

    public Register toEntity(RegisterReq req) throws Exception {
        Register register = new Register();
        register.setUserId(req.getUserId());
        register.setDate(stringToLocalDate(req.getDate()));
        register.setUnits(req.getUnits());
        register.setType(getType(req));
        register.setComments(req.getComments());
        register.setItem(findItemById(req.getItemId()));
        Long id = req.getAssignmentProjectId();
        register.setAssignmentProject(id == null ? null : finAssignmentProjectById(id));
        return register;
    }
    public RegisterRes toDto(Register register) {
        RegisterRes res = new RegisterRes();
        res.setId(register.getId());
        res.setDate(localDateToString(register.getDate()));
        res.setUnits(register.getUnits());
        res.setType(register.getType());
        res.setComments(register.getComments());

        AssignmentProject assignmentProject = register.getAssignmentProject();
        Long id = assignmentProject == null ? null : assignmentProject.getId();
        res.setAssignmentProjectId(id);

        res.setItemId(register.getItem().getId());
        return res;
    }

    public Register merge(Register register, RegisterReq req) throws Exception {
        register.setUserId(req.getUserId());
        register.setDate(stringToLocalDate(req.getDate()));
        register.setUnits(req.getUnits());
        register.setType(getType(req));
        register.setComments(req.getComments());
        register.setItem(findItemById(req.getItemId()));
        Long id = req.getAssignmentProjectId();
        register.setAssignmentProject(id == null ? null : finAssignmentProjectById(id));
        return register;
    }

    private static String getType(RegisterReq req) {
        String res = "";
        if (req.getAssignmentProjectId() == null) {
            res = RegisterType.INPUT_ITEM.toString();
        } else {
            res = RegisterType.OUTPUT_ITEM.toString();
        }
        return res;
    }

    private Item findItemById(Long id) throws Exception {
        Optional<Item> itemOptional = itemRepository.findItemById(id);
        if (itemOptional.isEmpty()) {
            throw new Exception("RegisterMapper: there is not found item with id " + id);
        }
        return itemOptional.get();
    }

    private AssignmentProject finAssignmentProjectById(Long id) throws Exception {
        Optional<AssignmentProject> assignmentProjectOptional = assignmentProjectRepository.findById(id);
        if (assignmentProjectOptional.isEmpty()) {
            throw new Exception("RegisterMapper : there is not found assignmentProject with id " + id);
        }
        return assignmentProjectOptional.get();
    }

    public RegisterReq buildRegisterReq(AssignmentProject assignProject){
        RegisterReq registerReq = new RegisterReq();
        registerReq.setUserId(assignProject.getUserId());
        registerReq.setDate(localDateToString(assignProject.getDate()));
        registerReq.setUnits(assignProject.getUnits());
        registerReq.setComments(assignProject.getComments());
        registerReq.setAssignmentProjectId(assignProject.getId());
        registerReq.setItemId(assignProject.getItem().getId());
        return registerReq;
    }

    private static String localDateToString(LocalDate date) {
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatters);
    }

    private static LocalDate stringToLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(dateString, formatter);
        return date;
    }
}
