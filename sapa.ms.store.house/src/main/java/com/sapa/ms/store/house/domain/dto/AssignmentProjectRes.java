package com.sapa.ms.store.house.domain.dto;

import com.sapa.ms.store.house.domain.domainMS.user.dto.ProjectRes;
import com.sapa.ms.store.house.domain.domainMS.user.dto.UserRes;
import lombok.Setter;

@Setter
public class AssignmentProjectRes {
    public Long id;
    public Long itemId;
    public Integer units;
    public String date;
    public String comments;
    public UserRes userRes;
    public ProjectRes project;
}
