package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.dto.AssignmentProjectReq;
import com.sapa.ms.store.house.domain.dto.AssignmentProjectRes;

import java.util.List;

public interface AssignmentProjectService {
    AssignmentProjectRes findById(Long id) throws Exception;

    List<AssignmentProjectRes> findByProjectId(Long id) throws Exception;

    AssignmentProjectRes create(AssignmentProjectReq req) throws Exception;

    AssignmentProjectRes update(Long id, AssignmentProjectReq req) throws Exception;

    void delete(Long id) throws Exception;

    List<AssignmentProjectRes> findAssignmentByItemId(Long id);
}
