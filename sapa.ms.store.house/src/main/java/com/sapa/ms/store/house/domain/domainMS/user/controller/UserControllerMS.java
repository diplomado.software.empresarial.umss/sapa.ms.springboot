package com.sapa.ms.store.house.domain.domainMS.user.controller;

import com.sapa.ms.store.house.domain.domainMS.user.dto.UserRes;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "user-ms", url = "localhost:8092/user/ms/usr")
public interface UserControllerMS {
    @GetMapping("/{id}")
    UserRes findById(@Valid @PathVariable(name = "id") Long id) throws Exception;
}
