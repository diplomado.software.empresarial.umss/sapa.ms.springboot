package com.sapa.ms.store.house.controllers;

import com.sapa.ms.store.house.domain.dto.ItemReq;
import com.sapa.ms.store.house.domain.dto.ItemRes;
import com.sapa.ms.store.house.services.ItemService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("store/house/ms/item")
@Tag(name = "ITEM-MS API")
public class ItemController {
    @Autowired
    private ItemService itemService;

    /**
     * @param id
     * @return ResponseEntity<ItemRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<ItemRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        ItemRes item = itemService.findById(id);
        return ResponseEntity.ok().body(item);
    }

    /**
     * @param req
     * @return ResponseEntity<ItemRes>
     * @throws Exception
     */
    @PostMapping("")
    public ResponseEntity<ItemRes> create(@Valid @RequestBody ItemReq req) throws Exception {
        ItemRes item = itemService.create(req);
        return ResponseEntity.ok().body(item);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<ItemRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<ItemRes> update(@Valid @PathVariable Long id,
                                          @RequestBody ItemReq req) throws Exception {
        ItemRes item = itemService.update(id, req);
        return ResponseEntity.ok().body(item);
    }

    /**
     * @param id
     * @return ResponseEntity<Void>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        itemService.delete(id);
        return ResponseEntity.ok().body(null);
    }

    /**
     * @param idStore
     * @return ResponseEntity<List < ItemRes>>
     */
    @GetMapping("/list/{id}")
    public ResponseEntity<List<ItemRes>> getItemListByStoreId(@Valid @PathVariable(name = "id") Long idStore) {
        List<ItemRes> itemList = itemService.getItemListByStoreId(idStore);
        return ResponseEntity.ok().body(itemList);
    }

    /**
     * @return ResponseEntity<List < ItemRes>>
     */
    @GetMapping("/list")
    public ResponseEntity<List<ItemRes>> findAll() {
        List<ItemRes> itemList = itemService.findAll();
        return ResponseEntity.ok().body(itemList);
    }
}
