package com.sapa.ms.store.house.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;

@Data
public class RegisterReq {
    @NotNull(message = "field userId cant not be null")
    @Positive(message = "userId can not be negative")
    public Long userId;
    @NotNull(message = "field date cant not be null")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    public String date;
    @NotNull(message = "field units cant not be null")
    @Positive(message = "units can not be negative")
    public Integer units;
    @NotNull(message = "field comments cant not be null")
    public String comments;
    public Long assignmentProjectId;
    @NotNull(message = "field itemId id cant not be null")
    @Positive(message = "itemId can not be negative")
    public Long itemId;
}
