package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.dto.RegisterReq;
import com.sapa.ms.store.house.domain.dto.RegisterRes;
import com.sapa.ms.store.house.domain.entities.Register;

import java.util.List;

public interface RegisterService {
    RegisterRes findById(Long id) throws Exception;

    RegisterRes create(RegisterReq req) throws Exception;

    RegisterRes update(Long id, RegisterReq req) throws Exception;

    List<RegisterRes> findRegisterByItemId(Long id) throws Exception;

    List<RegisterRes> findRegisterByItemIdAndType(Long id, String type) throws Exception;

    Register findRegisterByAssignmentProjectId(Long id) throws Exception;
}
