package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.domainMS.user.controller.ProjectControllerMS;
import com.sapa.ms.store.house.domain.domainMS.user.controller.UserControllerMS;
import com.sapa.ms.store.house.domain.domainMS.user.dto.ProjectRes;
import com.sapa.ms.store.house.domain.domainMS.user.dto.UserRes;
import com.sapa.ms.store.house.domain.dto.AssignmentProjectReq;
import com.sapa.ms.store.house.domain.dto.AssignmentProjectRes;
import com.sapa.ms.store.house.domain.dto.RegisterReq;
import com.sapa.ms.store.house.domain.entities.AssignmentProject;
import com.sapa.ms.store.house.domain.entities.Register;
import com.sapa.ms.store.house.domain.mapper.AssignmentProjectMapper;
import com.sapa.ms.store.house.domain.mapper.RegisterMapper;
import com.sapa.ms.store.house.repositories.AssignmentProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AssignmentProjectServiceImpl implements AssignmentProjectService {
    @Autowired
    private AssignmentProjectRepository assignmentProjectRepository;
    @Autowired
    private AssignmentProjectMapper assignmentProjectMapper;
    @Autowired
    private RegisterMapper registerMapper;
    @Autowired
    private RegisterService registerService;
    @Autowired
    private UserControllerMS userControllerMS;
    @Autowired
    private ProjectControllerMS projectControllerMS;

    @Override
    public AssignmentProjectRes findById(Long id) throws Exception {
        AssignmentProject entity = findAssignmentById(id);
        AssignmentProjectRes res = assignmentProjectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        ProjectRes projectRes = projectControllerMS.findById(entity.getProjectId());
        res.setUserRes(userRes);
        res.setProject(projectRes);
        return res;
    }

    @Override
    public List<AssignmentProjectRes> findByProjectId(Long id) throws Exception {
        List<AssignmentProject> assignmentProjectList = assignmentProjectRepository.findByProjectId(id);
        return assignmentProjectList
                .stream()
                .map(assignmentProjectMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public AssignmentProjectRes create(AssignmentProjectReq req) throws Exception {
        AssignmentProject entity = assignmentProjectMapper.toEntity(req);
        entity = assignmentProjectRepository.save(entity);
        RegisterReq registerReq = registerMapper.buildRegisterReq(entity);
        registerService.create(registerReq);
        AssignmentProjectRes res = assignmentProjectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        ProjectRes projectRes = projectControllerMS.findById(entity.getProjectId());
        res.setUserRes(userRes);
        res.setProject(projectRes);
        return res;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public AssignmentProjectRes update(Long id, AssignmentProjectReq req) throws Exception {
        AssignmentProject entity = findAssignmentById(id);
        entity = assignmentProjectMapper.merge(entity, req);
        assignmentProjectRepository.save(entity);
        Register register = registerService.findRegisterByAssignmentProjectId(id);
        RegisterReq registerReq = registerMapper.buildRegisterReq(entity);
        registerService.update(register.getId(),registerReq);
        AssignmentProjectRes res = assignmentProjectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUserRes(userRes);
        return res;
    }

    @Override
    public void delete(Long id) throws Exception {
        AssignmentProject assignmentProject = findAssignmentById(id);
        assignmentProjectRepository.delete(assignmentProject);
    }

    @Override
    public List<AssignmentProjectRes> findAssignmentByItemId(Long id) {
        List<AssignmentProject> assignmentProjectList = assignmentProjectRepository.findAssignmentByItemId(id);
        return assignmentProjectList
                .stream()
                .map(entity -> {
                    try {
                        AssignmentProjectRes res = assignmentProjectMapper.toDto(entity);
                        //UserRes userRes = userControllerMS.findById(entity.getUserId());
                        //ProjectRes projectRes = projectControllerMS.findById(entity.getProjectId());
                        //res.setProject(projectRes);
                        //res.setUserRes(userRes);
                        return res;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    private AssignmentProject findAssignmentById(Long id) throws Exception {
        Optional<AssignmentProject> assignmentOptional = assignmentProjectRepository.findById(id);
        if (assignmentOptional.isEmpty()) {
            throw new Exception("There is not found assignment with id " + id);
        }
        AssignmentProject entity = assignmentOptional.get();
        return entity;
    }
}
