package com.sapa.ms.store.house.services;

import com.sapa.ms.store.house.domain.dto.ItemReq;
import com.sapa.ms.store.house.domain.dto.ItemRes;
import com.sapa.ms.store.house.domain.entities.Item;
import com.sapa.ms.store.house.domain.entities.Store;
import com.sapa.ms.store.house.domain.mapper.ItemMapper;
import com.sapa.ms.store.house.repositories.ItemRepository;
import com.sapa.ms.store.house.repositories.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private StoreService storeService;
    @Autowired
    private ItemMapper itemMapper;

    @Override
    public ItemRes findById(Long id) throws Exception {
        Item item = findItemById(id);
        return itemMapper.toDto(item);
    }

    @Override
    public ItemRes create(ItemReq req) throws Exception {
        Item item = itemMapper.toEntity(req);
        Store store = storeService.findStoreById(req.storeId);
        item.setStore(store);
        item = itemRepository.save(item);
        return itemMapper.toDto(item);
    }

    @Override
    public ItemRes update(Long id, ItemReq req) throws Exception {
        Item item = findItemById(id);
        Store store = storeService.findStoreById(req.storeId);
        item = itemMapper.merge(item, req);
        item.setStore(store);
        item = itemRepository.save(item);
        return itemMapper.toDto(item);
    }

    @Override
    public void delete(Long id) throws Exception {
        Item item = findItemById(id);
        itemRepository.delete(item);
    }

    @Override
    public List<ItemRes> getItemListByStoreId(Long idStore) {
        List<Item> itemList = itemRepository.findItemByStoreId(idStore);
        return itemList
                .stream()
                .map(item -> itemMapper.toDto(item))
                .collect(Collectors.toList());
    }

    @Override
    public List<ItemRes> findAll() {
        List<Item> itemList = itemRepository.findAll();
        return itemList
                .stream()
                .map(item -> itemMapper.toDto(item))
                .collect(Collectors.toList());
    }

    private Item findItemById(Long id) throws Exception {
        Optional<Item> itemOptional = itemRepository.findById(id);
        if (itemOptional.isEmpty()) {
            throw new Exception("ItemServiceImpl: there is not found item with id " + id);
        }
        return itemOptional.get();
    }

    public void addToStock(Long id,Integer units) throws Exception {
        Item item = findItemById(id);
        item.setStock(item.getStock()+units);
        itemRepository.save(item);
    }

    public void removeFromStock(Long id,Integer units) throws Exception {
        Item item = findItemById(id);
        if(units<=item.getStock()){
            item.setStock(item.getStock()-units);
        }else {
            throw new Exception("Actually stock value:" + item.getStock() + " is lower than units " + units + " , for assignment to proyect");
        }
    }
}
