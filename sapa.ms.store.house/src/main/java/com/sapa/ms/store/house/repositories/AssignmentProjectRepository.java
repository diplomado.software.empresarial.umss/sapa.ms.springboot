package com.sapa.ms.store.house.repositories;


import com.sapa.ms.store.house.domain.entities.AssignmentProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssignmentProjectRepository extends JpaRepository<AssignmentProject, Long> {
    Optional<AssignmentProject> findById(Long id);
    List<AssignmentProject> findAssignmentByItemId(Long id);
    List<AssignmentProject> findByProjectId(Long id);
}
