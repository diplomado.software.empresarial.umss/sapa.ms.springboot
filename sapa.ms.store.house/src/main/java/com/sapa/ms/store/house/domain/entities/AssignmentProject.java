package com.sapa.ms.store.house.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Data
@Entity
@Table(name = "assignment_project")
public class AssignmentProject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user_id")
    private Long userId; // user-mss
    @Column(name = "project_id")
    private Long projectId; // project-ms
    private Integer units;
    private LocalDate date;
    private String comments;
    @ManyToOne
    @JoinColumn(name = "item_id", nullable = false)
    private Item item;
    @OneToOne(mappedBy = "assignmentProject")
    private Register register;
}
