# REST API IN A MICROSERVICES ARCHITECTURE WITH SPRING BOOT (SAPA)

<p align="center" with="100%">
  <img with="100%" src="https://gitlab.com/diplomado.software.empresarial.umss/sapa.ms.springboot/-/raw/develop/utils/images/arquitectura_ms.png?ref_type=heads"/>
</p>

## MICROSERVICES CONFIGURATIONS

### MICROSERVICE SERVER CONFIG
```bash
Name : config-server
Port : 8095
```

### MICROSERVICE EUREKA
```bash
Name : eureka-ms
Port : 8761
```

### MICROSERVICE API GATEWAY
```bash
Name : gateway-ms
Port : 9000
Paths :       
    - id: user-ms
        uri: http://localhost:8092
        predicates:
            - Path=/user/**
    - id: store-house-ms
        uri: http://localhost:8093
        predicates:
            - Path=/store/**
    - id: project-ms
        uri: http://localhost:8094
        predicates:
            - Path=/project/**
```

### MICROSERVICE USER
```bash
Name : user-ms
Port : 8092
Path : /user/**
```

### MICROSERVICIO STORE-HOUSE
```bash
Name : store-house-ms
Port : 8093
Path : /store/**
```

### MICROSERVICIO PROJECT
```bash
Name : project-ms
Port : 8094
Path : /project/**
```

## MICROSERVICES DATABASE MODEL
### MICROSERVICE USER
<p align="center" with="50%">
  <img with="100%" src="https://gitlab.com/diplomado.software.empresarial.umss/sapa.ms.springboot/-/raw/develop/utils/images/user_ms.png?ref_type=heads"/>
</p>

### MICROSERVICE STORE-HOUSE
<p align="center" with="50%">
  <img with="100%" src="https://gitlab.com/diplomado.software.empresarial.umss/sapa.ms.springboot/-/raw/develop/utils/images/store-house-ms.png?ref_type=heads"/>
</p>

### MICROSERVICE PROJECT
<p align="center" with="50%">
  <img with="100%" src="https://gitlab.com/diplomado.software.empresarial.umss/sapa.ms.springboot/-/raw/develop/utils/images/project-ms.png?ref_type=heads"/>
</p>

## TECHNOLOGIES 

#### Programming language
- [ ] [JDK 17](https://www.oracle.com/java/technologies/downloads/)

#### Framework 
- [ ] [Spring Boot 3.2.2](https://start.spring.io/)

#### Code editor
- [ ] [Intellij Idea](https://www.jetbrains.com/idea/)

#### Repository
- [ ] [GitLab](https://gitlab.com/)

#### Control system
- [ ] [Git](https://www.git-scm.com/)

#### Database system
- [ ] [Postgres](https://www.postgresql.org/)
- [ ] [MySQL](https://dev.mysql.com/downloads/workbench/)

#### Database manager
- [ ] [MySQL Workbench](https://dev.mysql.com/downloads/workbench/)
- [ ] [pgAadmin](https://www.pgadmin.org/)
- [ ] [DBeaver ](https://dbeaver.io/)

#### API platform
- [ ] [Postman](https://www.postman.com/)
- [ ] [Swagger](https://swagger.io/)


## STEPS FOR RUN PROJECT
```bash
1.- Download project at gitlab:
  - http https://gitlab.com/gersonEgues/test-proyect-forn-md-file.git
  - ssh git@gitlab.com:gersonEgues/test-proyect-forn-md-file.git
2.- Once in your project run
  $ npm install
3.- Star microservices in the following order
  - Config microservice
  - Eureka microservice
  - Gateway microservice
  - Project microservice
  - User microservice
  - Store-House microservice
```


## ADDITIONAL INFORMATION

### Developed by
- Author - **Egüesst CL**
- [Linkedin](https://www.linkedin.com/in/eguesst/)
- [GitLab](https://gitlab.com/gersonEgues) 
- [GitHub](https://github.com/GersonEgues)

## LICENSE
Open source


Thank you for visiting mine project! If you have any questions or suggestions, please do not hesitate to contact me.

