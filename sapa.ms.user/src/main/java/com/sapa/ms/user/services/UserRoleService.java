package com.sapa.ms.user.services;

import com.sapa.ms.user.domain.dto.RoleRes;

import java.util.List;

public interface UserRoleService {
    List<RoleRes> findRolesFromUser(Long id);
    void deleteAllUserRoleByUser(Long idUser);
}
