package com.sapa.ms.user.repository;

import com.sapa.ms.user.domain.entities.User;
import com.sapa.ms.user.domain.entities.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole,Long> {
    Optional<UserRole> findUserById(Long id);
    List<UserRole> findAllByUserId(Long id);
}
