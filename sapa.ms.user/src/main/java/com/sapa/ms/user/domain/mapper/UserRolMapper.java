package com.sapa.ms.user.domain.mapper;

import com.sapa.ms.user.domain.entities.Role;
import com.sapa.ms.user.domain.entities.User;
import com.sapa.ms.user.domain.entities.UserRole;
import org.springframework.stereotype.Component;

@Component
public class UserRolMapper {
    public static UserRole toEntity(User user, Role role) {
        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(role);
        return userRole;
    }
}
