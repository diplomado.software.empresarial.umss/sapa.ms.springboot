package com.sapa.ms.user.domain.mapper;

import com.sapa.ms.user.domain.dto.UserReq;
import com.sapa.ms.user.domain.dto.UserRes;
import com.sapa.ms.user.domain.entities.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public static UserRes toDto(User user) {
        UserRes res = new UserRes();
        res.setId(user.getId());
        res.setName(user.getName());
        res.setLastName(user.getLastName());
        res.setState(user.getState());
        return res;
    }

    public static User toEntity(UserReq req) {
        User user = new User();
        user.setName(capitalize(req.getName()));
        user.setLastName(capitalize(req.getLastName()));
        user.setState(true);
        return user;
    }

    public static User merge(User user,UserReq req){
        user.setName(req.getName());
        user.setLastName(req.getLastName());
        return user;
    }

    private static String capitalize(String string){
        String res = "";
        String array[] = string.trim().split(" ");
        for (String s : array) {
            res += s.substring(0,1).toUpperCase() + s.substring(1).toLowerCase() + " ";
        }
        res = res.substring(0,res.length()-1);
        return res;
    }
}
