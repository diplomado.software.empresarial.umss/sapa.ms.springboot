package com.sapa.ms.user.domain.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Getter;

@Getter
public class PageReq {
    @NotNull(message = "page can't be null")
    @PositiveOrZero(message = "page can't be negative")
    public Integer pageInit;
    @NotNull(message = "size can't be null")
    @PositiveOrZero(message = "size can't be negative")
    public Integer size;
    @NotNull(message = "sortableBy can't be null")
    public String sortableBy;
}
