package com.sapa.ms.user.controllers;

import com.sapa.ms.user.domain.dto.CountRes;
import com.sapa.ms.user.domain.dto.PageReq;
import com.sapa.ms.user.domain.dto.UserReq;
import com.sapa.ms.user.domain.dto.UserRes;
import com.sapa.ms.user.services.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user/ms/usr")
@Tag(name = "USER-MS API")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * @param req
     * @return
     */
    @PostMapping()
    public ResponseEntity<UserRes> createUser(@Valid @RequestBody UserReq req) {
        UserRes res = userService.create(req);
        return ResponseEntity.ok().body(res);
    }

    /**
     * @param id
     * @return ResponseEntity<UserRes>
     * @throws Exception
     */
    @GetMapping("{id}")
    public ResponseEntity<UserRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        UserRes res = userService.findById(id);
        return ResponseEntity.ok().body(res);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<UserRes>
     * @throws Exception
     */
    @PutMapping("{id}")
    public ResponseEntity<UserRes> updateUser(@Valid @PathVariable Long id,
                                              @Valid @RequestBody UserReq req) throws Exception {
        UserRes userRes = userService.update(id, req);
        return ResponseEntity.ok(userRes);
    }

    /**
     * @param state
     * @param pageReq
     * @return ResponseEntity<List < UserRes>>
     */
    @GetMapping("list/pageable/{state}")
    public ResponseEntity<List<UserRes>> findAllPageable(@Valid @PathVariable Boolean state,
                                                         @Valid @RequestBody PageReq pageReq) {
        List<UserRes> userResList = userService.findAllPageable(pageReq, state);
        return ResponseEntity.ok(userResList);
    }

    /**
     * @param state
     * @return ResponseEntity<List < UserRes>>
     */
    @GetMapping("list/{state}")
    public ResponseEntity<List<UserRes>> findAll(@Valid @PathVariable Boolean state) {
        List<UserRes> userResList = userService.findAll(state);
        return ResponseEntity.ok(userResList);
    }

    /**
     * @param id
     * @param state
     * @return ResponseEntity<UserRes>
     * @throws Exception
     */
    @PutMapping("state/{satate}")
    public ResponseEntity<UserRes> changeStateFromUser(@Valid @PathVariable Long id,
                                                       @Valid @PathVariable Boolean state) throws Exception {
        UserRes userRes = userService.changeStateFromUser(id, state);
        return ResponseEntity.ok(userRes);
    }

    /**
     * @param state
     * @return ResponseEntity<CountRes>
     */
    @GetMapping("count/{state}")
    public ResponseEntity<CountRes> getNumberOfUsers(@Valid @PathVariable Boolean state) {
        CountRes countRes = userService.getNumberOfUsers(state);
        return ResponseEntity.ok(countRes);
    }
}
