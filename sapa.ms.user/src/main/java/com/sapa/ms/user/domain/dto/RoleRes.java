package com.sapa.ms.user.domain.dto;

import lombok.Setter;

@Setter
public class RoleRes {
    public Long id;
    public String name;
    public Integer priority;
}
