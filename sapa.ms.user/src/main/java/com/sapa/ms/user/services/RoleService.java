package com.sapa.ms.user.services;

import com.sapa.ms.user.domain.dto.RoleReq;
import com.sapa.ms.user.domain.dto.RoleRes;
import com.sapa.ms.user.domain.entities.Role;

import java.util.List;

public interface RoleService {
    RoleRes findRole(Long id) throws Exception;

    RoleRes createRole(RoleReq req);

    RoleRes update(Long id, RoleReq roleReq) throws Exception;

    void delete(Long id) throws Exception;

    List<RoleRes> findAll();

    RoleRes updateRole(Long id, RoleReq req) throws Exception;

    public Role findRoleById(Long id) throws Exception;
}
