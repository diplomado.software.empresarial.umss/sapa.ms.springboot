package com.sapa.ms.user.domain.dto;

import lombok.Setter;

@Setter
public class CountRes {
    public Long count;
}
