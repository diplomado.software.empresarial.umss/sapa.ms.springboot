package com.sapa.ms.user.services;

import com.sapa.ms.user.domain.dto.CountRes;
import com.sapa.ms.user.domain.dto.PageReq;
import com.sapa.ms.user.domain.dto.UserReq;
import com.sapa.ms.user.domain.dto.UserRes;
import com.sapa.ms.user.domain.entities.User;

import java.util.List;

public interface UserService {
    UserRes findById(Long id) throws Exception;

    UserRes create(UserReq req);

    UserRes update(Long id, UserReq req) throws Exception;

    User findUserById(Long id) throws Exception;

    List<UserRes> findAll(Boolean state);

    UserRes changeStateFromUser(Long id,Boolean state) throws Exception;
    List<UserRes> findAllPageable(PageReq pageReq, Boolean state);

    CountRes getNumberOfUsers(Boolean state);
}
