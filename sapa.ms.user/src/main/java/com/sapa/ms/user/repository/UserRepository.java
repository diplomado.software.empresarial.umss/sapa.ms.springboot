package com.sapa.ms.user.repository;

import com.sapa.ms.user.domain.entities.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findUserById(Long id);
    List<User> findAllByState(Boolean state);
    List<User> findAllByState(Boolean state,Pageable pageable);
    Long countAllByState(Boolean state);
}
