package com.sapa.ms.user.services;

import com.sapa.ms.user.domain.dto.RoleReq;
import com.sapa.ms.user.domain.dto.RoleRes;
import com.sapa.ms.user.domain.entities.Role;
import com.sapa.ms.user.domain.mapper.RoleMapper;
import com.sapa.ms.user.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public RoleRes findRole(Long id) throws Exception {
        Role role = findRoleById(id);
        RoleRes roleRes = roleMapper.toDto(role);
        return roleRes;
    }

    @Override
    public RoleRes createRole(RoleReq req) {
        Role role = roleMapper.toEntity(req);
        role = roleRepository.save(role);
        RoleRes res = roleMapper.toDto(role);
        return res;
    }

    @Override
    public RoleRes update(Long id, RoleReq roleReq) throws Exception {
        Role role = findRoleById(id);
        role = roleMapper.merge(role, roleReq);
        roleRepository.save(role);
        RoleRes roleRes = roleMapper.toDto(role);
        return roleRes;
    }

    @Override
    public void delete(Long id) throws Exception {
        Role role = findRoleById(id);
        roleRepository.delete(role);
    }

    @Override
    public List<RoleRes> findAll() {
        List<Role> roleList = roleRepository.findAll();
        List<RoleRes> resList = roleList.stream()
                .map(RoleMapper::toDto)
                .collect(Collectors.toList());
        return resList;
    }

    @Override
    @Transactional
    public RoleRes updateRole(Long id, RoleReq req) throws Exception {
        Optional<Role> roleOptional = roleRepository.findRoleById(id);
        if (roleOptional.isEmpty()){
            throw new Exception("id " + id + " does not exist");
        }
        return roleMapper.toDto(roleOptional.get());
    }

    @Override
    public Role findRoleById(Long id) throws Exception {
        Optional<Role> roleOptional = roleRepository.findRoleById(id);
        if (roleOptional.isEmpty()) {
            throw new Exception("There is not found role with id " + id);
        }
        return roleOptional.get();
    }
}
