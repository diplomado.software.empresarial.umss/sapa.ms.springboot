package com.sapa.ms.user.domain.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;

import java.util.List;

@Getter
public class UserReq {
    @NotNull(message = "name can't be null")
    public String name;
    @NotNull(message = "last name can't be null")
    public String lastName;
    @NotNull(message = "roleIdList can't be null")
    @NotEmpty(message = "roleIdList can't be empty")
    public List<Long> roleIdList;
}
