package com.sapa.ms.user.services;

import com.sapa.ms.user.domain.dto.*;
import com.sapa.ms.user.domain.entities.Role;
import com.sapa.ms.user.domain.entities.User;
import com.sapa.ms.user.domain.entities.UserRole;
import com.sapa.ms.user.domain.mapper.UserMapper;
import com.sapa.ms.user.domain.mapper.UserRolMapper;
import com.sapa.ms.user.repository.RoleRepository;
import com.sapa.ms.user.repository.UserRepository;
import com.sapa.ms.user.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserRoleService userRoleService;

    @Override
    public UserRes findById(Long id) throws Exception {
        UserRes userRes;
        User user = findUserById(id);
        List<RoleRes> roleResList = userRoleService.findRolesFromUser(user.getId());
        userRes = userMapper.toDto(user);
        userRes.setRoleList(roleResList);
        return userRes;
    }

    @Override
    @Transactional
    public UserRes create(UserReq req) {
        UserRes userRes;
        User user = userMapper.toEntity(req);
        user = userRepository.save(user);
        userRes = saveUser(user, req);
        return userRes;
    }

    @Override
    @Transactional
    public UserRes update(Long id, UserReq req) throws Exception {
        UserRes userRes;
        User user = findUserById(id);
        user = userMapper.merge(user, req);
        userRoleService.deleteAllUserRoleByUser(id);
        userRes = saveUser(user, req);
        return userRes;
    }

    @Override
    public User findUserById(Long id) throws Exception {
        User user;
        Optional<User> userOptional = userRepository.findUserById(id);
        if (userOptional.isEmpty()) {
            throw new Exception("UserServiceImpl: there is not found user with id " + id);
        }
        user = userOptional.get();
        return user;
    }

    @Override
    public List<UserRes> findAllPageable(PageReq pageReq, Boolean state) {
        Pageable pageable = PageRequest.of(pageReq.pageInit, pageReq.size, Sort.by(pageReq.sortableBy));
        List<User> userList = userRepository.findAllByState(state, pageable);
        List<UserRes> userResList = userList
                .stream()
                .map(user -> buildUserRes(userMapper.toDto(user)))
                .collect(Collectors.toList());
        return userResList;
    }

    @Override
    public CountRes getNumberOfUsers(Boolean state) {
        Long numberOfUsers = userRepository.countAllByState(state);
        CountRes countRes = new CountRes();
        countRes.setCount(numberOfUsers);
        return countRes;
    }

    @Override
    public List<UserRes> findAll(Boolean state) {
        List<User> userList = userRepository.findAllByState(state);
        List<UserRes> userResList = userList
                .stream()
                .map(user -> buildUserRes(userMapper.toDto(user)))
                .collect(Collectors.toList());
        return userResList;
    }

    @Override
    public UserRes changeStateFromUser(Long id, Boolean state) throws Exception {
        UserRes userRes;
        User user = findUserById(id);
        user.setState(state);
        userRes = saveUser(user);
        return userRes;
    }

    private UserRes saveUser(User user, UserReq req) {
        UserRes userRes;
        List<Role> roleList = getRoleList(req.getRoleIdList());
        createUserRoleRelation(user, roleList);
        userRes = buildUserRes(userMapper.toDto(user));
        return userRes;
    }

    private UserRes saveUser(User user) {
        UserRes userRes;
        user = userRepository.save(user);
        userRes = userMapper.toDto(user);
        return userRes;
    }

    private UserRes buildUserRes(UserRes userRes) {
        List<RoleRes> roleResList = userRoleService.findRolesFromUser(userRes.id);
        userRes.setRoleList(roleResList);
        return userRes;
    }

    private List<Role> getRoleList(List<Long> idRoleList) {
        List<Role> roleList = idRoleList.stream().distinct().map(idRole -> roleRepository.findRoleById(idRole)).filter(roleOpt -> roleOpt.isPresent()).map(role -> role.get()).collect(Collectors.toList());
        return roleList;
    }

    private void createUserRoleRelation(User user, List<Role> roleList) {
        for (Role role : roleList) {
            UserRole userRole = UserRolMapper.toEntity(user, role);
            userRole = userRoleRepository.save(userRole);
        }
    }
}
