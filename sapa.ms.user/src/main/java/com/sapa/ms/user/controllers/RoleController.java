package com.sapa.ms.user.controllers;

import com.sapa.ms.user.domain.dto.RoleReq;
import com.sapa.ms.user.domain.dto.RoleRes;
import com.sapa.ms.user.services.RoleService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("user/ms/role")
@Tag(name = "ROLE-MS API")
public class RoleController {
    @Autowired
    private RoleService roleService;

    /**
     * @param req
     * @return ResponseEntity<RoleRes>
     */
    @PostMapping()
    public ResponseEntity<RoleRes> createRole(@Valid @RequestBody RoleReq req) {
        RoleRes res = roleService.createRole(req);
        return ResponseEntity.ok().body(res);
    }

    /**
     * @param id
     * @return ResponseEntity<RoleRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<RoleRes> findRole(@Valid @PathVariable(name = "id") Long id) throws Exception {
        RoleRes res = roleService.findRole(id);
        return ResponseEntity.ok().body(res);
    }

    /**
     * @param id
     * @param roleReq
     * @return ResponseEntity<RoleRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<RoleRes> update(@Valid @PathVariable(name = "id") Long id,
                                          @Valid @RequestBody RoleReq roleReq) throws Exception {
        RoleRes roleRes = roleService.update(id, roleReq);
        return ResponseEntity.ok().body(roleRes);
    }

    /**
     * @param id
     * @return ResponseEntity<Void>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        roleService.delete(id);
        return ResponseEntity.ok().body(null);
    }

    /**
     * @return ResponseEntity<List < RoleRes>>
     */
    @GetMapping("/list")
    public ResponseEntity<List<RoleRes>> findAll() {
        List<RoleRes> resList = roleService.findAll();
        return ResponseEntity.ok().body(resList);
    }
}
