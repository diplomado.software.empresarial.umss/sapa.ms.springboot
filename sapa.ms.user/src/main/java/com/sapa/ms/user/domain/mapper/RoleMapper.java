package com.sapa.ms.user.domain.mapper;

import com.sapa.ms.user.domain.dto.RoleReq;
import com.sapa.ms.user.domain.dto.RoleRes;
import com.sapa.ms.user.domain.entities.Role;
import org.springframework.stereotype.Component;

@Component
public class RoleMapper {
    public static RoleRes toDto(Role role) {
        RoleRes res = new RoleRes();
        res.setId(role.getId());
        res.setName(role.getName());
        res.setPriority(role.getPriority());
        return res;
    }

    public static Role toEntity(RoleReq req) {
        Role role = new Role();
        role.setName(req.getName().toUpperCase());
        role.setPriority(req.getPriority());
        return role;
    }

    public static Role merge(Role role, RoleReq req) {
        role.setName(req.getName());
        role.setPriority(req.getPriority());
        return role;
    }
}
