package com.sapa.ms.user.services;

import com.sapa.ms.user.domain.dto.RoleRes;
import com.sapa.ms.user.domain.entities.Role;
import com.sapa.ms.user.domain.entities.UserRole;
import com.sapa.ms.user.domain.mapper.RoleMapper;
import com.sapa.ms.user.repository.RoleRepository;
import com.sapa.ms.user.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserRolServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<RoleRes> findRolesFromUser(Long id) {
        List<UserRole> userRoleList = userRoleRepository.findAllByUserId(id);
        List<RoleRes> roleList = userRoleList
                .stream()
                .map(userRole -> getRoleRes(userRole))
                .filter(userRol -> userRol != null)
                .sorted(Comparator.comparing(role -> role.priority))
                .collect(Collectors.toList());
        return roleList;
    }

    private RoleRes getRoleRes(UserRole userRole) {
        RoleRes roleRes = null;
        Optional<Role> optionalRole = roleRepository.findRoleById(userRole.getRole().getId());
        if (optionalRole.isPresent()) {
            Role role = optionalRole.get();
            roleRes = roleMapper.toDto(role);
        }
        return roleRes;
    }

    @Transactional
    public void deleteAllUserRoleByUser(Long idUser) {
        List<UserRole> userRoleList = userRoleRepository.findAllByUserId(idUser);
        userRoleList.forEach(userRole -> {
            userRoleRepository.delete(userRole);
        });
    }
}
