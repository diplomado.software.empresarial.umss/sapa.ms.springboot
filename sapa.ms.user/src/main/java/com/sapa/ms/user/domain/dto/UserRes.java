package com.sapa.ms.user.domain.dto;

import lombok.Setter;

import java.util.List;

@Setter
public class UserRes {
    public Long id;
    public String name;
    public String lastName;
    public Boolean state;
    public List<RoleRes> roleList;
}
