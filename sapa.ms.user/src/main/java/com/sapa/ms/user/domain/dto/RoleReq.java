package com.sapa.ms.user.domain.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Getter;

@Getter
public class RoleReq {
    @NotNull(message = "name role can not be null")
    public String name;
    @NotNull(message = "priority can not be null")
    @PositiveOrZero(message = "priority can not be negative")
    public Integer priority;
}
