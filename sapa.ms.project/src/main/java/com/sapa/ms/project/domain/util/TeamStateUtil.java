package com.sapa.ms.project.domain.util;

import com.sapa.ms.project.domain.enums.ProjectState;
import org.springframework.stereotype.Component;

@Component
public class TeamStateUtil {
    public static String findStateForIU(String state) throws Exception {
        String response = "";
        if (ProjectState.PENDING.getNameBD().equals(state)) {
            response = ProjectState.PENDING.getNameIU();
        } else if (ProjectState.APPROVED.getNameBD().equals(state)) {
            response = ProjectState.APPROVED.getNameIU();
        } else if (ProjectState.STOPPED.getNameBD().equals(state)) {
            response = ProjectState.STOPPED.getNameIU();
        } else if (ProjectState.REJECTED.getNameBD().equals(state)) {
            response = ProjectState.REJECTED.getNameIU();
        } else {
            throw new Exception("ProjectServiceImpl: there is not found state with name " + state);
        }
        return response;
    }

    public static String findStateForBE(String state) throws Exception {
        String response = "";
        if (ProjectState.PENDING.getNameIU().equals(state)) {
            response = ProjectState.PENDING.getNameBD();
        } else if (ProjectState.APPROVED.getNameIU().equals(state)) {
            response = ProjectState.APPROVED.getNameBD();
        } else if (ProjectState.STOPPED.getNameIU().equals(state)) {
            response = ProjectState.STOPPED.getNameBD();
        } else if (ProjectState.REJECTED.getNameIU().equals(state)) {
            response = ProjectState.REJECTED.getNameBD();
        } else {
            throw new Exception("ProjectServiceImpl: there is not found state with name " + state);
        }
        return response;
    }
}
