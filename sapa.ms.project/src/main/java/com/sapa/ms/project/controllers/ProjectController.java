package com.sapa.ms.project.controllers;

import com.sapa.ms.project.domain.dto.ProjectReq;
import com.sapa.ms.project.domain.dto.ProjectRes;
import com.sapa.ms.project.services.ProjectService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("project/ms/project")
@Tag(name = "PROJECT-MS API")
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    /**
     * @param id
     * @return ResponseEntity<ProjectRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<ProjectRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        ProjectRes projectRes = projectService.findById(id);
        return ResponseEntity.ok().body(projectRes);
    }

    /**
     *
     * @param id
     * @return ResponseEntity<ProjectRes>
     * @throws Exception
     */
    @GetMapping("/assigned/item/{id}")
    public ResponseEntity<ProjectRes> findByIdWithAssignedItems(@Valid @PathVariable(name = "id") Long id) throws Exception {
        ProjectRes projectRes = projectService.findByIdWithAssignedItems(id);
        return ResponseEntity.ok().body(projectRes);
    }

    /**
     * @param req
     * @return ResponseEntity<ProjectRes>
     * @throws Exception
     */
    @PostMapping()
    public ResponseEntity<ProjectRes> create(@Valid @RequestBody ProjectReq req) throws Exception {
        ProjectRes projectRes = projectService.create(req);
        return ResponseEntity.ok().body(projectRes);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<ProjectRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<ProjectRes> update(@Valid @PathVariable(name = "id") Long id,
                                             @Valid @RequestBody ProjectReq req) throws Exception {
        ProjectRes projectRes = projectService.update(id, req);
        return ResponseEntity.ok().body(projectRes);
    }

    /**
     * @param id
     * @return ResponseEntity<ProjectRes>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<ProjectRes> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        ProjectRes projectRes = projectService.delete(id);
        return ResponseEntity.ok().body(projectRes);
    }

    /**
     * @param startDate
     * @param endDate
     * @param state
     * @return ResponseEntity<List < ProjectRes>>
     * @throws Exception
     */
    @GetMapping("/list/{startDate}/{endDate}/{state}")
    public ResponseEntity<List<ProjectRes>> findProjectByStartDateAndEndDateAndState(@Valid @PathVariable(name = "startDate") String startDate,
                                                                                     @Valid @PathVariable(name = "endDate") String endDate,
                                                                                     @Valid @PathVariable(name = "state") String state) throws Exception {
        List<ProjectRes> projectResList = projectService.findProjectByStartDateAndEndDateAndState(startDate, endDate, state);
        return ResponseEntity.ok().body(projectResList);
    }

    /**
     * @param state
     * @return ResponseEntity<List < ProjectRes>>
     * @throws Exception
     */
    @GetMapping("/list/{state}")
    public ResponseEntity<List<ProjectRes>> findProjectByState(@Valid @PathVariable(name = "state") String state) throws Exception {
        List<ProjectRes> projectResList = projectService.findProjectByState(state);
        return ResponseEntity.ok().body(projectResList);
    }
}
