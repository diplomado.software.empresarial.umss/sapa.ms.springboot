package com.sapa.ms.project.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProjectReq {
    public Long managerId;// user-ms
    public String name;
    public String state;
    public String startDate;
    public String endDate;
    public String description;
}
