package com.sapa.ms.project.controllers;

import com.sapa.ms.project.domain.dto.PersonReq;
import com.sapa.ms.project.domain.dto.PersonRes;
import com.sapa.ms.project.services.PersonService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("project/ms/person")
@Tag(name = "PERSON-MS API")
public class PersonController {
    @Autowired
    private PersonService personService;

    /**
     * @param id
     * @return ResponseEntity<PersonRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<PersonRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        PersonRes personRes = personService.findById(id);
        return ResponseEntity.ok().body(personRes);
    }

    /**
     * @param req
     * @return ResponseEntity<PersonRes>
     * @throws Exception
     */
    @PostMapping("")
    public ResponseEntity<PersonRes> create(@Valid @RequestBody PersonReq req) throws Exception {
        PersonRes personRes = personService.create(req);
        return ResponseEntity.ok().body(personRes);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<PersonRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<PersonRes> update(@Valid @PathVariable(name = "id") Long id,
                                            @Valid @RequestBody PersonReq req) throws Exception {
        PersonRes personRes = personService.update(id, req);
        return ResponseEntity.ok().body(personRes);
    }

    /**
     * @param id
     * @return ResponseEntity<Void>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        Void nothing = personService.delete(id);
        return ResponseEntity.ok().body(nothing);
    }

    /**
     * @param id
     * @param state
     * @return ResponseEntity<PersonRes>
     * @throws Exception
     */
    @PutMapping("/{id}/{state}")
    public ResponseEntity<PersonRes> changeState(@Valid @PathVariable(name = "id") Long id,
                                                 @Valid @PathVariable(name = "state") Boolean state) throws Exception {
        PersonRes personRes = personService.changeState(id, state);
        return ResponseEntity.ok().body(personRes);
    }

    /**
     * @param id
     * @return ResponseEntity<List < PersonRes>>
     */
    @GetMapping("/list/{id}")
    public ResponseEntity<List<PersonRes>> findAllByTeamId(@Valid @PathVariable(name = "id") Long id) {
        List<PersonRes> list = personService.findAllByTeamId(id);
        return ResponseEntity.ok().body(list);
    }
}
