package com.sapa.ms.project.domain.dto;

import com.sapa.ms.project.domain.domainMS.user.dto.UserRes;
import jakarta.persistence.Column;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonRes {
    public Long id;
    public String name;
    public String dni;
    public String contractDate;
    public Boolean state;
    public UserRes user;
}
