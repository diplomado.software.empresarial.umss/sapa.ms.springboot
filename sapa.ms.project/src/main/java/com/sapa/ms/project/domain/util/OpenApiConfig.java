package com.sapa.ms.project.domain.util;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition(
        info = @Info(
                title = "API FOR PROJECT-MS",
                version = "1.0.0",
                description = "API for Project Microservice from SAPA"
        )
)
public class OpenApiConfig {

}
