package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.dto.PersonReq;
import com.sapa.ms.project.domain.dto.PersonRes;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PersonService {
    PersonRes findById(Long id) throws Exception;

    PersonRes create(PersonReq req) throws Exception;

    PersonRes update(Long id, PersonReq req) throws Exception;

    Void delete(Long id) throws Exception;

    PersonRes changeState(Long id, Boolean state) throws Exception;

    List<PersonRes> findAllByTeamId(Long id);
}
