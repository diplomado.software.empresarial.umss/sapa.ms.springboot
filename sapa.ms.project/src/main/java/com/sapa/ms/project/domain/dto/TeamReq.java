package com.sapa.ms.project.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeamReq {
    public String name;
    public String state;
    public Long capatazId;// ms-user
    public Long projectId;
    public Long vehicleId;
}
