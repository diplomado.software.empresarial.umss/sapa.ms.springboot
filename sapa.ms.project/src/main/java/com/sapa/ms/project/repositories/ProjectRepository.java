package com.sapa.ms.project.repositories;

import com.sapa.ms.project.domain.entities.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {
    Optional<Project> findById(Long id);
    List<Project> findProjectByStartDateGreaterThanEqualAndEndDateLessThanEqualAndState(LocalDate startDate, LocalDate endDate, String state);

    List<Project> findProjectByState(String state) throws Exception;
}
