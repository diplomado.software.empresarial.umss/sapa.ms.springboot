package com.sapa.ms.project.domain.mapper;

import com.sapa.ms.project.domain.dto.TeamReq;
import com.sapa.ms.project.domain.dto.TeamRes;
import com.sapa.ms.project.domain.entities.Team;
import com.sapa.ms.project.domain.util.TeamStateUtil;
import org.springframework.stereotype.Component;

@Component
public class TeamMapper {
    public static Team toEntity(TeamReq req) throws Exception {
        Team entity = new Team();
        entity.setName(req.getName());
        entity.setState(TeamStateUtil.findStateForBE(req.getState()));
        entity.setCapatazId(req.getCapatazId());
        return entity;
    }

    public static TeamRes toDto(Team entity) throws Exception {
        TeamRes res = new TeamRes();
        res.setId(entity.getId());
        res.setName(entity.getName());
        res.setState(TeamStateUtil.findStateForIU(entity.getState()));
        return res;
    }

    public static Team merge(Team entity, TeamReq req) throws Exception {
        entity.setName(req.getName());
        entity.setState(TeamStateUtil.findStateForBE(req.getState()));
        entity.setCapatazId(req.getCapatazId());
        return entity;
    }
}
