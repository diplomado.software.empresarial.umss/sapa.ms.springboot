package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.dto.TeamReq;
import com.sapa.ms.project.domain.dto.TeamRes;
import com.sapa.ms.project.domain.entities.Team;

import java.util.List;

public interface TeamService {
    TeamRes findById(Long id) throws Exception;

    Team findTeamById(Long id) throws Exception;

    TeamRes create(TeamReq req) throws Exception;

    TeamRes update(Long id, TeamReq req) throws Exception;

    Void delete(Long id) throws Exception;

    List<TeamRes> findTeamByProjectId(Long id);
}
