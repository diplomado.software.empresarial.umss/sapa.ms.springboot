package com.sapa.ms.project.domain.enums;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum ProjectState {
    PENDING("Pendiente", "PENDING"),
    APPROVED("Aprobado", "APPROVED"),
    REJECTED("Rechazado", "REJECTED"),
    STOPPED("Paralizado", "STOPPED");

    private String nameIU;
    private String nameBD;

    ProjectState(String nameIU, String nameBD) {
        this.nameIU = nameIU;
        this.nameBD = nameBD;
    }
}
