package com.sapa.ms.project.controllers;


import com.sapa.ms.project.domain.dto.TeamReq;
import com.sapa.ms.project.domain.dto.TeamRes;
import com.sapa.ms.project.services.TeamService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("project/ms/team")
@Tag(name = "TEAM-MS API")
public class TeamController {
    @Autowired
    private TeamService teamService;

    /**
     * @param id
     * @return ResponseEntity<TeamRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<TeamRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        TeamRes teamRes = teamService.findById(id);
        return ResponseEntity.ok().body(teamRes);
    }

    /**
     * @param req
     * @return ResponseEntity<TeamRes>
     * @throws Exception
     */
    @PostMapping("")
    public ResponseEntity<TeamRes> create(@Valid @RequestBody TeamReq req) throws Exception {
        TeamRes teamRes = teamService.create(req);
        return ResponseEntity.ok().body(teamRes);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<TeamRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<TeamRes> update(@Valid @PathVariable(name = "id") Long id,
                                          @Valid @RequestBody TeamReq req) throws Exception {
        TeamRes teamRes = teamService.update(id, req);
        return ResponseEntity.ok().body(teamRes);
    }

    /**
     * @param id
     * @return ResponseEntity<Void>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        Void nothing = teamService.delete(id);
        return ResponseEntity.ok().body(nothing);
    }

    /**
     * @param id
     * @return ResponseEntity<List < TeamRes>>
     */
    @GetMapping("/list/{id}")
    public ResponseEntity<List<TeamRes>> findTeamByProjectId(@Valid @PathVariable(name = "id") Long id) {
        List<TeamRes> list = teamService.findTeamByProjectId(id);
        return ResponseEntity.ok().body(list);
    }
}
