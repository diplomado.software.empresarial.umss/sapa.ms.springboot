package com.sapa.ms.project.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleRes {
    public Long id;
    public String plaque;
    public String model;
    public String color;
}
