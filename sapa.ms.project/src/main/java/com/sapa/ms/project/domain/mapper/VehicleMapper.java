package com.sapa.ms.project.domain.mapper;

import com.sapa.ms.project.domain.dto.VehicleReq;
import com.sapa.ms.project.domain.dto.VehicleRes;
import com.sapa.ms.project.domain.entities.Vehicle;
import org.springframework.stereotype.Component;

@Component
public class VehicleMapper {
    public static Vehicle toEntity(VehicleReq req) {
        Vehicle entity = new Vehicle();
        entity.setPlaque(req.getPlaque());
        entity.setModel(req.getModel());
        entity.setColor(req.getColor());
        return entity;
    }

    public static VehicleRes toDto(Vehicle entity) {
        VehicleRes res = new VehicleRes();
        res.setId(entity.getId());
        res.setPlaque(entity.getPlaque());
        res.setModel(entity.getModel());
        res.setColor(entity.getColor());
        return res;
    }

    public static Vehicle merge(Vehicle entity, VehicleReq req) {
        entity.setPlaque(req.getPlaque());
        entity.setModel(req.getModel());
        entity.setColor(req.getColor());
        return entity;
    }
}
