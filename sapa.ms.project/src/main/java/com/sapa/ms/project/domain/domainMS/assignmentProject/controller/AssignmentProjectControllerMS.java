package com.sapa.ms.project.domain.domainMS.assignmentProject.controller;

import com.sapa.ms.project.domain.domainMS.assignmentProject.dto.AssignmentProjectRes;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "assignment-project-ms", url = "localhost:8093/store/house/ms/assignment/project")
public interface AssignmentProjectControllerMS {
    @GetMapping("/{id}")
    List<AssignmentProjectRes> findByProjectId(@Valid @PathVariable(name = "id") Long id) throws Exception;
}
