package com.sapa.ms.project.domain.domainMS.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRes {
    public Long id;
    public String name;
    public String lastName;
    public Boolean state;
    public List<RoleRes> roleList;
}
