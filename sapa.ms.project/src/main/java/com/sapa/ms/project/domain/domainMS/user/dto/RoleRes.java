package com.sapa.ms.project.domain.domainMS.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleRes {
    public Long id;
    public String name;
    public Integer priority;
}
