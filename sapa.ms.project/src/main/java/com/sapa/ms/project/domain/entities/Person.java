package com.sapa.ms.project.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;

@Data
@Entity
@Table(name = "person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user_id")
    private Long userId;
    @Column()
    private String name;
    @Column()
    private String dni;
    @Column(name = "contract_date")
    private LocalDate contractDate;
    private Boolean state;
    @ManyToOne()
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;
}
