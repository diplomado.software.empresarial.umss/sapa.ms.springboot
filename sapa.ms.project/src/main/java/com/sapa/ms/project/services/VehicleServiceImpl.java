package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.dto.VehicleReq;
import com.sapa.ms.project.domain.dto.VehicleRes;
import com.sapa.ms.project.domain.entities.Vehicle;
import com.sapa.ms.project.domain.mapper.VehicleMapper;
import com.sapa.ms.project.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VehicleServiceImpl implements VehicleService {
    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private VehicleMapper vehicleMapper;

    @Override
    public VehicleRes findById(Long id) throws Exception {
        Vehicle vehicle = finVehicleById(id);
        return vehicleMapper.toDto(vehicle);
    }

    @Override
    public Vehicle finVehicleById(Long id) throws Exception {
        Optional<Vehicle> vehicleOptional = vehicleRepository.findById(id);
        if (vehicleOptional.isEmpty()) {
            throw new Exception("VehicleServiceImpl: there is not found vehicle with id " + id);
        }
        return vehicleOptional.get();
    }

    @Override
    public VehicleRes create(VehicleReq req) {
        Vehicle vehicle = vehicleMapper.toEntity(req);
        vehicle = vehicleRepository.save(vehicle);
        return vehicleMapper.toDto(vehicle);
    }

    @Override
    public VehicleRes update(Long id, VehicleReq req) throws Exception {
        Vehicle vehicle = finVehicleById(id);
        vehicle = vehicleMapper.merge(vehicle, req);
        vehicle = vehicleRepository.save(vehicle);
        return vehicleMapper.toDto(vehicle);
    }

    @Override
    public Void delete(Long id) throws Exception {
        Vehicle vehicle = finVehicleById(id);
        vehicleRepository.delete(vehicle);
        return null;
    }

    @Override
    public List<VehicleRes> findAll() {
        List<VehicleRes> list = vehicleRepository.findAll()
                .stream()
                .map(VehicleMapper::toDto)
                .collect(Collectors.toList());
        return list;
    }
}
