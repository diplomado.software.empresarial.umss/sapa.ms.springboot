package com.sapa.ms.project.domain.mapper;

import com.sapa.ms.project.domain.dto.PersonReq;
import com.sapa.ms.project.domain.dto.PersonRes;
import com.sapa.ms.project.domain.entities.Person;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


@Component
public class PersonMapper {
    public static Person toEntity(PersonReq req) {
        Person entity = new Person();
        entity.setUserId(req.getUserId());
        entity.setName(req.getName());
        entity.setDni(req.getDni());
        entity.setContractDate(stringToLocalDate(req.getContractDate()));
        entity.setState(req.getState());
        return entity;
    }

    public static PersonRes toDto(Person entity) {
        PersonRes res = new PersonRes();
        res.setId(entity.getId());
        res.setName(entity.getName());
        res.setDni(entity.getDni());
        res.setContractDate(localDateToString(entity.getContractDate()));
        res.setState(entity.getState());
        return res;
    }

    public static Person merge(Person entity, PersonReq req) {
        entity.setUserId(req.getUserId());
        entity.setName(req.getName());
        entity.setDni(req.getDni());
        entity.setContractDate(stringToLocalDate(req.getContractDate()));
        entity.setState(req.getState());
        return entity;
    }

    private static String localDateToString(LocalDate date) {
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatters);
    }

    private static LocalDate stringToLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(dateString, formatter);
        return date;
    }
}
