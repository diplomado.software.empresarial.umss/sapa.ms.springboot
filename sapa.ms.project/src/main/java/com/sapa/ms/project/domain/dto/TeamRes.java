package com.sapa.ms.project.domain.dto;

import com.sapa.ms.project.domain.domainMS.user.dto.UserRes;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class TeamRes {
    public Long id;
    public String name;
    public String state;
    public ProjectRes project;
    public VehicleRes vehicle;
    public List<PersonRes> personList;
    public UserRes capataz;
}
