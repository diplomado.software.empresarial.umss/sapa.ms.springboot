package com.sapa.ms.project.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehicleReq {
    private String plaque;
    private String model;
    private String color;
}
