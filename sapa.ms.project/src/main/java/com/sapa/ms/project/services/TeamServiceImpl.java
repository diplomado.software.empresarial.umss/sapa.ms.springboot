package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.domainMS.user.controller.UserControllerMS;
import com.sapa.ms.project.domain.domainMS.user.dto.UserRes;
import com.sapa.ms.project.domain.dto.*;
import com.sapa.ms.project.domain.entities.Project;
import com.sapa.ms.project.domain.entities.Team;
import com.sapa.ms.project.domain.entities.Vehicle;
import com.sapa.ms.project.domain.mapper.PersonMapper;
import com.sapa.ms.project.domain.mapper.ProjectMapper;
import com.sapa.ms.project.domain.mapper.TeamMapper;
import com.sapa.ms.project.domain.mapper.VehicleMapper;
import com.sapa.ms.project.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TeamServiceImpl implements TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TeamMapper teamMapper;
    @Autowired
    private ProjectMapper projectMapper;
    @Autowired
    private VehicleMapper vehicleMapper;
    @Autowired
    private ProjectService projectService;
    @Autowired
    private VehicleService vehicleService;
    @Autowired
    private UserControllerMS userControllerMS;

    @Override
    public TeamRes findById(Long id) throws Exception {
        Team entity = findTeamById(id);
        TeamRes res = teamMapper.toDto(entity);
        setResponseDependenciesForTeam(entity,res);
        UserRes userRes = userControllerMS.findById(entity.getCapatazId());
        res.setCapataz(userRes);
        return res;
    }

    @Override
    public Team findTeamById(Long id) throws Exception {
        Optional<Team> teamOptional = teamRepository.findById(id);
        if (teamOptional.isEmpty()) {
            throw new Exception("TeamServiceImpl: there is not found team with id " + id);
        }
        return teamOptional.get();
    }

    @Override
    public TeamRes create(TeamReq req) throws Exception {
        Team entity = teamMapper.toEntity(req);
        setEntitiesDependenciesForTeam(entity, req);
        entity = teamRepository.save(entity);
        TeamRes res = teamMapper.toDto(entity);
        setResponseDependenciesForTeam(entity, res);
        UserRes userRes = userControllerMS.findById(entity.getCapatazId());
        res.setCapataz(userRes);
        return res;
    }

    private void setEntitiesDependenciesForTeam(Team team, TeamReq req) throws Exception {
        Project project = projectService.findProjectById(req.getProjectId());
        Vehicle vehicle = vehicleService.finVehicleById(req.getVehicleId());
        team.setProject(project);
        team.setVehicle(vehicle);
    }

    private void setResponseDependenciesForTeam(Team team, TeamRes res) throws Exception {
        ProjectRes projectRes = projectMapper.toDto(team.getProject());
        VehicleRes vehicleRes = vehicleMapper.toDto(team.getVehicle());
        List<PersonRes> personResList = new ArrayList<>();
        if(team.getPerson()!=null){
            personResList = team.getPerson().stream().map(PersonMapper::toDto).collect(Collectors.toList());
        }
        res.setProject(projectRes);
        res.setVehicle(vehicleRes);
        res.setPersonList(personResList);

        //get user-ms
        UserRes managerProjectRes = userControllerMS.findById(team.getProject().getManagerId());
        projectRes.setManager(managerProjectRes);
    }

    @Override
    public TeamRes update(Long id, TeamReq req) throws Exception {
        Team entity = findTeamById(id);
        entity = teamMapper.merge(entity, req);
        setEntitiesDependenciesForTeam(entity, req);
        entity = teamRepository.save(entity);
        TeamRes res = teamMapper.toDto(entity);
        setResponseDependenciesForTeam(entity, res);
        UserRes userRes = userControllerMS.findById(entity.getCapatazId());
        res.setCapataz(userRes);
        return res;
    }

    @Override
    public Void delete(Long id) throws Exception {
        Team team = findTeamById(id);
        teamRepository.delete(team);
        return null;
    }

    @Override
    public List<TeamRes> findTeamByProjectId(Long id) {
        List<Team> teamList = teamRepository.findTeamByProjectId(id);
        return teamList
                .stream()
                .map(entity -> {
                    try {
                        TeamRes res = teamMapper.toDto(entity);
                        setResponseDependenciesForTeam(entity, res);
                        UserRes userRes = userControllerMS.findById(entity.getCapatazId());
                        res.setCapataz(userRes);
                        return res;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }
}
