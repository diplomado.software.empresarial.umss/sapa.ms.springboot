package com.sapa.ms.project.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.util.List;

@Data
@Entity
@Table(name = "team")
public class Team {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String state;

    @Column(name = "capataz_id")
    private Long capatazId; // ms-user

    @ManyToOne()
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;
    @OneToOne(cascade = CascadeType.REMOVE, orphanRemoval = true,
            fetch = FetchType.LAZY)
    @JoinColumn(name = "vehicle_id", referencedColumnName = "id")
    private Vehicle vehicle;
    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true,
            mappedBy = "team", fetch = FetchType.LAZY)
    private List<Person> person;
}
