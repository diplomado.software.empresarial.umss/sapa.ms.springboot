package com.sapa.ms.project.domain.dto;

import com.sapa.ms.project.domain.domainMS.assignmentProject.dto.AssignmentProjectRes;
import com.sapa.ms.project.domain.domainMS.user.dto.UserRes;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProjectRes {
    public Long id;
    public String name;
    public String state;
    public String startDate;
    public String endDate;
    public String description;
    public UserRes manager;
    public List<AssignmentProjectRes> assignmentProjectResList;
}
