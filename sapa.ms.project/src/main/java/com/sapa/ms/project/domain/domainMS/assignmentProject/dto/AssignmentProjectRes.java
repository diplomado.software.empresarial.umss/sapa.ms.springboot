package com.sapa.ms.project.domain.domainMS.assignmentProject.dto;

import com.sapa.ms.project.domain.domainMS.user.dto.UserRes;
import com.sapa.ms.project.domain.dto.ProjectRes;
import lombok.Setter;

@Setter
public class AssignmentProjectRes {
    public Long id;
    public Long itemId;
    public Integer units;
    public String date;
    public String comments;
    public UserRes userRes;
    public ProjectRes project;
}
