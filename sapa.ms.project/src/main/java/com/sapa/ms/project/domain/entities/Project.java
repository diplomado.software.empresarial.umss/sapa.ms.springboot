package com.sapa.ms.project.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "manager_id")
    private Long managerId;// user-ms
    private String name;
    private String state;
    @Column(name = "start_Date")
    private LocalDate startDate;
    @Column(name = "    end_Date")
    private LocalDate endDate;
    private String description;
    @OneToMany(cascade = CascadeType.REMOVE, orphanRemoval = true,
            mappedBy = "project", fetch = FetchType.LAZY)
    private List<Team> teams;
}
