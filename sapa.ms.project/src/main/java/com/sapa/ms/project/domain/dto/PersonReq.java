package com.sapa.ms.project.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonReq {
    public Long userId;
    public String name;
    public String dni;
    public String contractDate;
    public Boolean state;
    public Long teamId;
}
