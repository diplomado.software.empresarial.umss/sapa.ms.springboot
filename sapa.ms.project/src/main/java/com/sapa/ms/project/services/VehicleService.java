package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.dto.VehicleReq;
import com.sapa.ms.project.domain.dto.VehicleRes;
import com.sapa.ms.project.domain.entities.Vehicle;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface VehicleService {
    VehicleRes findById(Long id) throws Exception;

    Vehicle finVehicleById(Long id) throws Exception;

    VehicleRes create(VehicleReq req);

    VehicleRes update(Long id, VehicleReq req) throws Exception;

    Void delete(Long id) throws Exception;

    List<VehicleRes> findAll();
}
