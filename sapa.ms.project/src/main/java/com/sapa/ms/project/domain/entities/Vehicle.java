package com.sapa.ms.project.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
@Table(name = "vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String plaque;
    private String model;
    private String color;
    @OneToOne(mappedBy = "vehicle")
    private Team team;
}
