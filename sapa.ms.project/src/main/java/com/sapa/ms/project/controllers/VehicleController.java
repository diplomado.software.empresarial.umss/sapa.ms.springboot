package com.sapa.ms.project.controllers;

import com.sapa.ms.project.domain.dto.VehicleReq;
import com.sapa.ms.project.domain.dto.VehicleRes;
import com.sapa.ms.project.services.VehicleService;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("project/ms/vehicle")
@Tag(name = "VEHICLE-MS API")
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;

    /**
     * @param id
     * @return ResponseEntity<VehicleRes>
     * @throws Exception
     */
    @GetMapping("/{id}")
    public ResponseEntity<VehicleRes> findById(@Valid @PathVariable(name = "id") Long id) throws Exception {
        VehicleRes vehicleRes = vehicleService.findById(id);
        return ResponseEntity.ok().body(vehicleRes);
    }

    /**
     * @param req
     * @return ResponseEntity<VehicleRes>
     */
    @PostMapping("")
    public ResponseEntity<VehicleRes> create(@Valid @RequestBody VehicleReq req) {
        VehicleRes vehicleRes = vehicleService.create(req);
        return ResponseEntity.ok().body(vehicleRes);
    }

    /**
     * @param id
     * @param req
     * @return ResponseEntity<VehicleRes>
     * @throws Exception
     */
    @PutMapping("/{id}")
    public ResponseEntity<VehicleRes> update(@Valid @PathVariable(name = "id") Long id,
                                             @RequestBody VehicleReq req) throws Exception {
        VehicleRes vehicleRes = vehicleService.update(id, req);
        return ResponseEntity.ok().body(vehicleRes);
    }

    /**
     * @param id
     * @return ResponseEntity<Void>
     * @throws Exception
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@Valid @PathVariable(name = "id") Long id) throws Exception {
        Void nothing = vehicleService.delete(id);
        return ResponseEntity.ok().body(nothing);
    }

    /**
     * @return ResponseEntity<List < VehicleRes>>
     */
    @GetMapping("/list")
    public ResponseEntity<List<VehicleRes>> findByModelList() {
        List<VehicleRes> list = vehicleService.findAll();
        return ResponseEntity.ok().body(list);
    }
}
