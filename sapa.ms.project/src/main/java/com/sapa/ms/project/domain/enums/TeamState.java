package com.sapa.ms.project.domain.enums;

import lombok.Getter;

@Getter
public enum TeamState {
    PENDING("Pendiente", "PENDING"),
    APPROVED("Aprobado", "APPROVED"),
    REJECTED("Rechazado", "REJECTED"),
    STOPPED("Paralizado", "STOPPED");


    private String nameIU;
    private String nameBD;

    TeamState(String nameIU, String nameBD) {
        this.nameIU = nameIU;
        this.nameBD = nameBD;
    }
}
