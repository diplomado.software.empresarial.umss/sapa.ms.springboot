package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.domainMS.user.controller.UserControllerMS;
import com.sapa.ms.project.domain.domainMS.user.dto.UserRes;
import com.sapa.ms.project.domain.dto.PersonReq;
import com.sapa.ms.project.domain.dto.PersonRes;
import com.sapa.ms.project.domain.entities.Person;
import com.sapa.ms.project.domain.entities.Team;
import com.sapa.ms.project.domain.mapper.PersonMapper;
import com.sapa.ms.project.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private PersonMapper personMapper;
    @Autowired
    private TeamService teamService;
    @Autowired
    private UserControllerMS userControllerMS;

    @Override
    public PersonRes findById(Long id) throws Exception {
        Person entity = findPersonlById(id);
        PersonRes res = personMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUser(userRes);
        return res;
    }

    private Person findPersonlById(Long id) throws Exception {
        Optional<Person> personOptional = personRepository.findById(id);
        if (personOptional.isEmpty()) {
            throw new Exception("PersonServiceImpl: there is not found person with id " + id);
        }
        return personOptional.get();
    }

    @Override
    public PersonRes create(PersonReq req) throws Exception {
        Person entity = personMapper.toEntity(req);
        Team team = teamService.findTeamById(req.getTeamId());
        entity.setTeam(team);
        entity = personRepository.save(entity);
        PersonRes res = personMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUser(userRes);
        return res;
    }

    @Override
    public PersonRes update(Long id, PersonReq req) throws Exception {
        Person entity = findPersonlById(id);
        entity = personMapper.merge(entity, req);
        Team team = teamService.findTeamById(req.getTeamId());
        entity.setTeam(team);
        personRepository.save(entity);
        PersonRes res = personMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUser(userRes);
        return res;
    }

    @Override
    public Void delete(Long id) throws Exception {
        Person person = findPersonlById(id);
        personRepository.delete(person);
        return null;
    }

    @Override
    public PersonRes changeState(Long id, Boolean state) throws Exception {
        Person entity = findPersonlById(id);
        entity.setState(state);
        entity = personRepository.save(entity);
        PersonRes res = personMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getUserId());
        res.setUser(userRes);
        return res;
    }

    @Override
    public List<PersonRes> findAllByTeamId(Long id) {
        List<PersonRes> list = personRepository.findAllByTeamId(id)
                .stream()
                .map(entity->{
                    try {
                        PersonRes res =  PersonMapper.toDto(entity);
                        UserRes userRes = userControllerMS.findById(entity.getUserId());
                        res.setUser(userRes);
                        return res;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
        return list;
    }
}
