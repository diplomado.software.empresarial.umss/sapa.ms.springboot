package com.sapa.ms.project.domain.mapper;

import com.sapa.ms.project.domain.dto.ProjectReq;
import com.sapa.ms.project.domain.dto.ProjectRes;
import com.sapa.ms.project.domain.entities.Project;
import com.sapa.ms.project.domain.util.ProjectStateUtil;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class ProjectMapper {
    public static Project toEntity(ProjectReq req) throws Exception {
        Project entity = new Project();
        entity.setManagerId(req.getManagerId());
        entity.setName(req.getName());
        entity.setState(ProjectStateUtil.findStateForBE(req.getState()));
        entity.setStartDate(stringToLocalDate(req.getStartDate()));
        entity.setEndDate(stringToLocalDate(req.getEndDate()));
        entity.setDescription(req.getDescription());
        return entity;
    }

    public static ProjectRes toDto(Project entity) throws Exception {
        ProjectRes res = new ProjectRes();
        res.setId(entity.getId());
        res.setName(entity.getName());
        res.setState(ProjectStateUtil.findStateForIU(entity.getState()));
        res.setStartDate(localDateToString(entity.getStartDate()));
        res.setEndDate(localDateToString(entity.getEndDate()));
        res.setDescription(entity.getDescription());
        return res;
    }

    public static Project merge(Project entity, ProjectReq req) throws Exception {
        entity.setManagerId(req.getManagerId());
        entity.setName(req.getName());
        entity.setState(ProjectStateUtil.findStateForBE(req.getState()));
        entity.setStartDate(stringToLocalDate(req.getStartDate()));
        entity.setEndDate(stringToLocalDate(req.getEndDate()));
        entity.setDescription(req.getDescription());
        return entity;
    }

    private static String localDateToString(LocalDate date) {
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatters);
    }

    private static LocalDate stringToLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(dateString, formatter);
        return date;
    }
}
