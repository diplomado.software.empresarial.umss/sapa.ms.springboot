package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.dto.ProjectReq;
import com.sapa.ms.project.domain.dto.ProjectRes;
import com.sapa.ms.project.domain.entities.Project;

import java.util.List;

public interface ProjectService {
    ProjectRes findById(Long id) throws Exception;

    ProjectRes findByIdWithAssignedItems(Long id) throws Exception;

    ProjectRes create(ProjectReq req) throws Exception;

    ProjectRes update(Long id, ProjectReq req) throws Exception;

    ProjectRes delete(Long id) throws Exception;

    List<ProjectRes> findProjectByStartDateAndEndDateAndState(String startDate, String endDate, String state) throws Exception;

    List<ProjectRes> findProjectByState(String state) throws Exception;

    Project findProjectById(Long id) throws Exception;
}
