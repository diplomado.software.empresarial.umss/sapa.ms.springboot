package com.sapa.ms.project.services;

import com.sapa.ms.project.domain.domainMS.assignmentProject.controller.AssignmentProjectControllerMS;
import com.sapa.ms.project.domain.domainMS.assignmentProject.dto.AssignmentProjectRes;
import com.sapa.ms.project.domain.domainMS.user.controller.UserControllerMS;
import com.sapa.ms.project.domain.domainMS.user.dto.UserRes;
import com.sapa.ms.project.domain.dto.ProjectReq;
import com.sapa.ms.project.domain.dto.ProjectRes;
import com.sapa.ms.project.domain.entities.Project;
import com.sapa.ms.project.domain.enums.ProjectState;
import com.sapa.ms.project.domain.mapper.ProjectMapper;
import com.sapa.ms.project.domain.util.ProjectStateUtil;
import com.sapa.ms.project.repositories.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private ProjectMapper projectMapper;
    @Autowired
    private UserControllerMS userControllerMS;
    @Autowired
    private AssignmentProjectControllerMS assignmentProjectControllerMS;

    @Override
    public ProjectRes findById(Long id) throws Exception {
        Project entity = findProjectById(id);
        ProjectRes res = projectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getManagerId());
        res.setManager(userRes);
        return res;
    }

    @Override
    public Project findProjectById(Long id) throws Exception {
        Optional<Project> project = projectRepository.findById(id);
        if (project.isEmpty()) {
            throw new Exception("ProjectServiceImpl: there is not found project with id " + id);
        }
        return project.get();
    }

    @Override
    public ProjectRes findByIdWithAssignedItems(Long id) throws Exception {
        Project entity = findProjectById(id);
        ProjectRes res = projectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getManagerId());
        List<AssignmentProjectRes> assignmentProjectResList = assignmentProjectControllerMS.findByProjectId(id);
        res.setManager(userRes);
        res.setAssignmentProjectResList(assignmentProjectResList);
        return res;
    }

    @Override
    public ProjectRes create(ProjectReq req) throws Exception {
        Project entity = ProjectMapper.toEntity(req);
        projectRepository.save(entity);
        ProjectRes res = projectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getManagerId());
        res.setManager(userRes);
        return res;
    }

    @Override
    public ProjectRes update(Long id, ProjectReq req) throws Exception {
        Project entity = findProjectById(id);
        projectMapper.merge(entity, req);
        projectRepository.save(entity);
        ProjectRes res = projectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getManagerId());
        res.setManager(userRes);
        return res;
    }

    @Override
    public ProjectRes delete(Long id) throws Exception {
        Project entity = findProjectById(id);
        entity.setState(ProjectState.REJECTED.getNameBD());
        entity = projectRepository.save(entity);
        ProjectRes res = ProjectMapper.toDto(entity);
        UserRes userRes = userControllerMS.findById(entity.getManagerId());
        res.setManager(userRes);
        return res;
    }

    @Override
    public List<ProjectRes> findProjectByStartDateAndEndDateAndState(String startDate, String endDate, String state) throws Exception {
        LocalDate startDateLD = formatStringToLocalDate(startDate);
        LocalDate endDateLD = formatStringToLocalDate(endDate);
        String projectState = ProjectStateUtil.findStateForBE(state);
        List<Project> projectList = projectRepository.findProjectByStartDateGreaterThanEqualAndEndDateLessThanEqualAndState(startDateLD, endDateLD, projectState);
        return projectList
                .stream()
                .map(entity -> {
                    try {
                        ProjectRes res = ProjectMapper.toDto(entity);
                        UserRes userRes = userControllerMS.findById(entity.getManagerId());
                        res.setManager(userRes);
                        return res;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<ProjectRes> findProjectByState(String state) throws Exception {
        String projectState = ProjectStateUtil.findStateForBE(state);
        List<Project> projectList = projectRepository.findProjectByState(projectState);
        return projectList
                .stream()
                .map(entity -> {
                    try {
                        ProjectRes res = ProjectMapper.toDto(entity);
                        UserRes userRes = userControllerMS.findById(entity.getManagerId());
                        res.setManager(userRes);
                        return res;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .collect(Collectors.toList());
    }

    private static LocalDate formatStringToLocalDate(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(dateString, formatter);
        return date;
    }
}
